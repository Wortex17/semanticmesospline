﻿using UnityEngine;
using System.Collections;

namespace SMS.Math
{
    /// <summary>
    /// Class describing a centripetal Catmull–Rom spline
    /// </summary>
    public abstract class CatmullRomSpline : CubicHermiteSpline
    {
        #region Catmull-Rom equations

        /// <summary>
        /// Solve the Catmull-Rom parametric equation for a given time(t) and vector quadruple (p1,p2,p3,p4)
        /// </summary>
        /// <param name="t"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="p4"></param>
        /// <returns></returns>
        public static Vector3 solve(float t, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
        {
            float t2 = t * t;
            float t3 = t2 * t;

            float b1 = 0.5f * (-t3 + 2f * t2 - t);
            float b2 = 0.5f * (3f * t3 - 5f * t2 + 2f);
            float b3 = 0.5f * (-3f * t3 + 4f * t2 + t);
            float b4 = 0.5f * (t3 - t2);

            return (p1 * b1 + p2 * b2 + p3 * b3 + p4 * b4);
        }

        /// <summary>
        /// Solves the first derivative of the equation
        /// </summary>
        /// <param name="t"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="p4"></param>
        /// <returns></returns>
        public static Vector3 solve1st(float t, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
        {
            float t2 = t * t;

            float b1 = 0.5f * (-3f * t2 + 4f * t - 1f);
            float b2 = 0.5f * (9f * t2 - 10f * t);
            float b3 = 0.5f * (-9f * t2 + 8f * t + 1f);
            float b4 = 0.5f * (3f * t2 - 2f * t);

            return (p1 * b1 + p2 * b2 + p3 * b3 + p4 * b4);
        }
        /// <summary>
        /// Solve the Catmull-Rom parametric equation for a given time(t) and vector quadruple (p1,p2,p3,p4)
        /// </summary>
        public static Vector3 solve(Segment segment, ControlPoint[] ControlPoints)
        {
            return solve(segment.LocalT, ControlPoints[segment.KnotIdx0], ControlPoints[segment.KnotIdx1], ControlPoints[segment.KnotIdx2], ControlPoints[segment.KnotIdx3]);
        }
        public static Vector3 solve1st(Segment segment, ControlPoint[] ControlPoints)
        {
            return solve1st(segment.LocalT, ControlPoints[segment.KnotIdx0], ControlPoints[segment.KnotIdx1], ControlPoints[segment.KnotIdx2], ControlPoints[segment.KnotIdx3]);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns the position got by solving with the Catmull-Rom equation or linear interpolation when only 2 CPs are present.
        /// When there is only one CP, returns that CP. If there are no CPs, returns a zero vector
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public override Vector3 getPositionAt(float t)
        {
            switch (ControlPoints.Count)
            {
                case 0:
                    return Vector3.zero;
                case 1:
                    return ControlPoints[0];
                case 2:
                    return Vector3.Lerp(ControlPoints[0], ControlPoints[1], t);
                default:
                    return solve(getSegmentAt(t), ControlPoints.ToArray());
            }
        }
        public override Vector3 get1stDerivativeAt(float t)
        {
            switch (ControlPoints.Count)
            {
                case 0:
                case 1:
                    return Vector3.zero;
                default:
                    return solve1st(getSegmentAt(t), ControlPoints.ToArray());
            }
        }

        #endregion
    }
}
