﻿using UnityEngine;
using System.Collections;

namespace SMS.Math
{
    /// <summary>
    /// Uses parallel transport to get derivatives from the spline
    /// </summary>
    public static class ParallelTransport
    {
        public static Vector3 getTangentAt(CubicHermiteSpline spline, float t)
        {
            return spline.get1stDerivativeAt(t).normalized;
        }
        public static Vector3 getNormalAt(CubicHermiteSpline spline, float t)
        {
            Vector3 tangent = getTangentAt(spline, t);
            Vector3 arbitraryAxis = Vector3.back;
            return Vector3.Cross(tangent, arbitraryAxis).normalized;
        }


        public static Vector3[] getHullNormalsAt(CubicHermiteSpline spline, float t, uint hullResolution)
        {
            Vector3[] hullNormals = new Vector3[hullResolution];

            Vector3 tangent = getTangentAt(spline, t);
            Vector3 normal = getNormalAt(spline, t);

            float rotationStepSize = (1f / (float)hullResolution) * 360f;

            for (int i = 0; i < hullResolution; i++)
            {
                hullNormals[i] = normal;
                normal = Quaternion.AngleAxis(rotationStepSize, tangent) * normal;
            }

            return hullNormals;
        }

        public static Vector3[][] getHullNormals(CubicHermiteSpline spline, float[] sampleSteps, uint hullResolution)
        {
            Vector3[][] hullNormals = new Vector3[sampleSteps.Length][];

            for (int step = 0; step < sampleSteps.Length; step++)
            {
                hullNormals[step] = getHullNormalsAt(spline, sampleSteps[step], hullResolution);
            }

            return hullNormals;
        }
    }
}