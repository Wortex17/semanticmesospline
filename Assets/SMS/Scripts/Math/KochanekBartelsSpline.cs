﻿using UnityEngine;
using System.Collections;

namespace SMS.Math
{
    /// <summary>
    /// Class describing a Kochanek-Bartels spline
    /// </summary>
    public abstract class KochanekBartelsSpline : CubicHermiteSpline
    {
        #region Kochanek-Bartels equations

        /// <summary>
        /// Solve the Kochanek-Bartels parametric equation for a given time(t) and vector quadruple (p1,p2,p3,p4)
        /// </summary>
        /// <param name="t"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="p4"></param>
        /// <returns></returns>
        public static Vector3 solve(float t, float Tension, float Bias, float Continuity, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
        {
            Vector3 tan1 = getKCB_IncomingTangent(Tension, Bias, Continuity, p1, p2, p3);
            Vector3 tan2 = getKCB_OutgoingTangent(Tension, Bias, Continuity, p2, p3, p4);
            return solveHermite(t, p2, p3, tan1, tan2);
        }

        public static Vector3 solve1st(float t, float Tension, float Bias, float Continuity, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
        {
            Vector3 tan1 = getKCB_IncomingTangent(Tension, Bias, Continuity, p1, p2, p3);
            Vector3 tan2 = getKCB_OutgoingTangent(Tension, Bias, Continuity, p2, p3, p4);
            return solveHermite1st(t, p2, p3, tan1, tan2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="t">Time value [0..1]</param>
        /// <param name="p0">Starting Point of the Curve</param>
        /// <param name="p1">End Point of the Curve</param>
        /// <param name="tan0">Tangent at starting Point of the Curve</param>
        /// <param name="tan1">Tangent at Ending Point of the Curve</param>
        /// <returns></returns>
        public static Vector3 solveHermite(float t, Vector3 p0, Vector3 p1, Vector3 tan0, Vector3 tan1)
        {
            //Storing exponents
            float t2 = t * t;
            float t3 = t2 * t;

            //Hermite Components
            float h1 = (2f * t3 - 3f * t2 + 1);
            float h2 = (-2f * t3 + 3f * t2);
            float h3 = (t3 - 2f * t2 + t);
            float h4 = (t3 - t2);

            return (h1 * p0 + h2 * p1 + h3 * tan0 + h4 * tan1);
        }

        public static Vector3 solveHermite1st(float t, Vector3 p0, Vector3 p1, Vector3 tan0, Vector3 tan1)
        {
            //Storing exponents
            float t2 = t * t;

            //Hermite Components
            float h1 = (5f*t2 - 5f*t);
            float h2 = (-5f*t2 + 5f*t);
            float h3 = (3f*t2 - 4f*t + 1f);
            float h4 = (3f*t2 - 2f*t);

            return (h1 * p0 + h2 * p1 + h3 * tan0 + h4 * tan1);
        }

        public static Vector3 getKCB_IncomingTangent(float Tension, float Bias, float Continuity, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            //Diretcly form wikipedia
            Vector3 tangent =
                (((1f - Tension) * (1f + Bias) * (1f + Continuity)) * 0.5f) * (p2 - p1)
                +
                (((1f - Tension) * (1f - Bias) * (1f - Continuity)) * 0.5f) * (p3 - p2)
                ;
            return tangent;
        }

        public static Vector3 getKCB_OutgoingTangent(float Tension, float Bias, float Continuity, Vector3 p2, Vector3 p3, Vector3 p4)
        {
            //Diretcly form wikipedia
            Vector3 tangent =
                (((1f - Tension) * (1f + Bias) * (1f - Continuity)) * 0.5f) * (p3 - p2)
                +
                (((1f - Tension) * (1f - Bias) * (1f + Continuity)) * 0.5f) * (p4 - p3)
                ;
            return tangent;
        }


        /// <summary>
        /// Solve the Kochanek-Bartels parametric equation for a given time(t) and vector quadruple (p1,p2,p3,p4)
        /// </summary>
        public static Vector3 solve(Segment segment, float Tension, float Bias, float Continuity, ControlPoint[] ControlPoints)
        {
            return solve(segment.LocalT, Tension, Bias, Continuity, ControlPoints[segment.KnotIdx0], ControlPoints[segment.KnotIdx1], ControlPoints[segment.KnotIdx2], ControlPoints[segment.KnotIdx3]);
        }

        public static Vector3 solve1st(Segment segment, float Tension, float Bias, float Continuity, ControlPoint[] ControlPoints)
        {
            return solve1st(segment.LocalT, Tension, Bias, Continuity, ControlPoints[segment.KnotIdx0], ControlPoints[segment.KnotIdx1], ControlPoints[segment.KnotIdx2], ControlPoints[segment.KnotIdx3]);
        }

        #endregion

        #region Public Properties

        [Range(-1f, 1f)]
        public float Tension = 0f;
        [Range(-1f, 1f)]
        public float Bias = 0f;
        [Range(-1f, 1f)]
        public float Continuity = 0f;

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns the position got by solving with the Kochanek-Bartels equation or linear interpolation when only 2 CPs are present.
        /// When there is only one CP, returns that CP. If there are no CPs, returns a zero vector
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public override Vector3 getPositionAt(float t)
        {
            switch (ControlPoints.Count)
            {
                case 0:
                    return Vector3.zero;
                case 1:
                    return ControlPoints[0];
                case 2:
                    return Vector3.Lerp(ControlPoints[0], ControlPoints[1], t);
                default:
                    return solve(getSegmentAt(t), Tension, Bias, Continuity, ControlPoints.ToArray());
            }
        }

        public override Vector3 get1stDerivativeAt(float t)
        {
            switch (ControlPoints.Count)
            {
                case 0:
                case 1:
                    return Vector3.zero;
                default:
                    return solve1st(getSegmentAt(t), Tension, Bias, Continuity, ControlPoints.ToArray());
            }
        }

        #endregion
    }
}