﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SMS.Math
{
    /// <summary>
    /// A base class for classes describing Cubic Hermite Splines with arbitrary amount of data points / control points
    /// </summary>
    /// <remarks>
    /// We are inehriting this form MonoBehaviour to faciliate unitys internal serializers etc., otherwise it would be too much
    /// of a hassel for the serialization is not part of our project
    /// </remarks>
    public abstract class CubicHermiteSpline : MonoBehaviour
    {
        #region Public Properties

        public List<ControlPoint> ControlPoints
        {
            get
            {
                return mControlPoints;
            }
            set
            {
                mControlPoints = value;
            }
        }

        public float ApproximateTotalLength
        {
            get
            {
                return mApproximateTotalLength;
            }
        }

        /// <summary>
        /// The BaseSize defines the inherited or absolute base-size,
        /// a factor applied to all radii of the cp's for the baseform
        /// </summary>
        public abstract float BaseSize
        {
            get;
        }

        #endregion

        #region Public Methods

        public abstract Vector3 getPositionAt(float t);
        public abstract Vector3 get1stDerivativeAt(float t);

        /// <summary>
        /// approximate the absolute length of the spline
        /// </summary>
        public float updateApproximateLength(int precision = 10)
        {
            precision = Mathf.Max(precision, 1);

            mApproximateLengths.Clear();
            mApproximateTotalLength = 0f;

            if (mControlPoints.Count > 2)
            {
                float segTLength = 1f / (mControlPoints.Count - 1);

                for (int i = 1; i < mControlPoints.Count; i++)
                {
                    float segEnd = segTLength * (float)i;
                    float segStart = segTLength * (float)(i - 1);
                    float segTpart = segEnd - segStart;

                    float segRealLength = 0f;

                    float segSampleStep = segTpart / precision;
                    Vector3 from = getPositionAt(segStart);
                    for (int s = 1; s <= precision; s++)
                    {
                        Vector3 to = getPositionAt(segStart + segSampleStep * s);

                        //Increase seglength by real distance
                        segRealLength += (to - from).magnitude;
                        
                        //set head as new starting point
                        from = to;
                    }
                    mApproximateLengths.Add(segRealLength);
                    mApproximateTotalLength += segRealLength;
                }
            }
            else if (mControlPoints.Count == 2)
            {
                float linearLength = (getPositionAt(1f) - getPositionAt(0f)).magnitude;
                mApproximateLengths.Add(linearLength);
                mApproximateTotalLength += linearLength;
            }

            

            return mApproximateTotalLength;
        }

        /// <summary>
        /// Takes a normalized distance (length) in the range [0..1] and converts it to the time parameter [0..1] on the curve
        /// Effectively gives us the time where the distance is traveled
        /// </summary>
        public float getTimeAtNormDistance(float targetDistance)
        {
            targetDistance = Mathf.Clamp01(targetDistance);
            float passedLength = 0f;

            //the time-length of any segment
            float segTLength = (mControlPoints.Count > 1) ? 1f / (mControlPoints.Count - 1) : 1f;

            for (int i = 0; i < mApproximateLengths.Count; i++)
            {
                float normSegLength = (mApproximateTotalLength > 0f) ? mApproximateLengths[i] / mApproximateTotalLength : 0f;

                if (targetDistance <= passedLength + normSegLength)
                {
                    float distanceIntoSegment = targetDistance - passedLength;
                    float passedTime = (float)(i) * segTLength;
                    //Add the timeIntoSegment, which is calculated by the distanceIntoSegment applied to the local segments length of time 
                    passedTime += (distanceIntoSegment / normSegLength) * segTLength;
                    return passedTime;
                }
                else
                {
                    passedLength += normSegLength;
                }
            }

            return 0f;
        }

        public float getRadiusAt(float t)
        {
            switch (ControlPoints.Count)
            {
                case 0:
                    return 0f;
                case 1:
                    return ControlPoints[0].Radius;
                case 2:
                    return Mathf.Lerp(ControlPoints[0].Radius, ControlPoints[1].Radius, t);
                default:
                    Segment seg = getSegmentAt(t);
                    return Mathf.Lerp(ControlPoints[seg.KnotIdx1].Radius, ControlPoints[seg.KnotIdx2].Radius, seg.LocalT);
            }
        }
        
        /// <summary>
        /// Returns an array of t-values for a sampling range with the defined resolution
        /// </summary>
        /// <remarks>
        /// This algorithm steps from 0, meaning that the final segment (1f) will NOT be included unless lastStep is true
        /// </remarks>
        /// <param name="resolution"></param>
        /// <returns></returns>
        static public float[] getSampleSteps(uint resolution, bool lastStep = false)
        {
            //In case resolution is 0, lastStep should not be included either
            if (resolution == 0)
            {
                lastStep = false;
            }
            float[] sample = new float[resolution + ((lastStep) ? 1 : 0)];
            float stepSize = 1f / (float)resolution;
            for (int step = 0; step < resolution; step++)
            {
                sample[step] = stepSize * (float)step;
            }
            if (lastStep)
            {
                sample[resolution] = 1f;
            }
            return sample;
        }

        /// <summary>
        /// Returns a sample using the stepping algorithm.
        /// The returned array will contain resolution+1 elements, unless fullSample is false, in which case it will contain exactly resolution elements.
        /// </summary>
        /// <param name="resolution"></param>
        /// <param name="fullSample">If false, the final step (1f) will NOT be included (pure stepping output)</param>
        /// <returns></returns>
        public Vector3[] getSamplePositions(uint resolution, bool fullSample = true)
        {
            float[] steps = getSampleSteps(resolution, fullSample);
            Vector3[] sample = new Vector3[steps.Length];
            int step = 0;
            foreach (float t in steps)
            {
                sample[step] = getPositionAt(t);
                step++;
            }
            return sample;
        }

        public float[] getSampleRadii(uint resolution, bool fullSample = true)
        {
            float[] steps = getSampleSteps(resolution, fullSample);
            float[] sample = new float[steps.Length];
            int step = 0;
            foreach (float t in steps)
            {
                sample[step] = getRadiusAt(t);
                step++;
            }
            return sample;
        }

        public Segment getSegmentAt(float t)
        {
            t = Mathf.Clamp01(t);

            //Defines at which index we allow the "definition" of a segment.
            //E.g. if 1, then the first CP will never be the begin of a drawable segment but only a outlyer for calculation
            int firstSegmentStartPointIdx = 0;
            //Amount of segments in this Spline
            //We allow 2-point segments, otherwise we would have to subtract 3 from CP count
            int segmentCount = ControlPoints.Count - (1 + 2*firstSegmentStartPointIdx);
            //Uniform segment length (part of [0..1] total spline length)
            float segmentLength = 1f / (float)segmentCount;
            //The segment index of the spline our t is in
            int targetSegment = Mathf.Max(Mathf.CeilToInt(t / segmentLength) - 1, 0);
            //Calculate the control point (indices) of the segment
            int p0, p1, p2, p3;
            //Start point of the target segment
            p1 = firstSegmentStartPointIdx + targetSegment;
            p2 = p1 + 1;
            //Get Next and previous point
            //May need securities and possible cheats
            p3 = Mathf.Min(p1 + 2, ControlPoints.Count - 1);
            p0 = Mathf.Max(p1 - 1, 0);

            //Map the t between p1 & p2 to get a new local t [0..1]
            float localT = Mathf.InverseLerp((float)(p1 - firstSegmentStartPointIdx) * segmentLength, (float)(p2 - firstSegmentStartPointIdx) * segmentLength, t);

            return new Segment(localT, p0, p1, p2, p3);
        }

        #endregion
        
        #region Public Types

        /// <summary>
        /// Describes a 4-point segment of a Cubic Hermite Spline, where the actual segment is form t0 to t1
        /// </summary>
        public class Segment
        {
            /// <summary>
            /// The localized t-value in this segment
            /// </summary>
            public float LocalT = Mathf.Infinity;
            /// <summary>
            /// The Index of the first Knot of the Segment / t0-1
            /// </summary>
            public int KnotIdx0 = -1;
            /// <summary>
            /// The Index of the second Knot of the Segment / t0
            /// </summary>
            public int KnotIdx1 = -1;
            /// <summary>
            /// The Index of the third Knot of the Segment / t1
            /// </summary>
            public int KnotIdx2 = -1;
            /// <summary>
            /// The Index of the fourth Knot of the Segment / t1+1
            /// </summary>
            public int KnotIdx3 = -1;

            public Segment(float localT, int knot0, int knot1, int knot2, int knot3)
            {
                LocalT = localT;
                KnotIdx0 = knot0;
                KnotIdx1 = knot1;
                KnotIdx2 = knot2;
                KnotIdx3 = knot3;
            }

            public Segment(int knot0, int knot1, int knot2, int knot3)
            {
                KnotIdx0 = knot0;
                KnotIdx1 = knot1;
                KnotIdx2 = knot2;
                KnotIdx3 = knot3;
            }

            public Segment(float localT, int knot1, int knot2)
            {
                LocalT = localT;

                KnotIdx0 = knot1;
                KnotIdx1 = knot1;
                KnotIdx2 = knot2;
                KnotIdx3 = knot2;
            }

            public Segment(int knot1, int knot2)
            {
                KnotIdx0 = knot1;
                KnotIdx1 = knot1;
                KnotIdx2 = knot2;
                KnotIdx3 = knot2;
            }

        }

        [System.Serializable]
        public struct ControlPoint
        {
            public Vector3 Position;
            public float Radius;

            public ControlPoint(Vector3 position, float radius = 1f)
            {
                Position = position;
                Radius = radius;
            }

            public static implicit operator Vector3(ControlPoint cp)
            {
                return cp.Position;
            }
        }

        #endregion

        #region Protected Fields

        [SerializeField, HideInInspector]
        List<ControlPoint> mControlPoints = new List<ControlPoint>();
        /// <summary>
        /// Stores the length for each segment (each control point after the first)
        /// </summary>
        [SerializeField, HideInInspector]
        List<float> mApproximateLengths = new List<float>();
        float mApproximateTotalLength = 0f;

        #endregion
    }
}