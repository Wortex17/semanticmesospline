﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SMS.MeshGeneration;

namespace SMS.Features
{
    public interface FeatureContainer 
    {
        /// <summary>
        /// Should return all feature points, sorted by ascending t-value (position along the spline)
        /// </summary>
        /// <returns></returns>
        List<FeaturePoint> getFeaturePoints();

        List<FeaturePoint> getFeaturePointsInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline);
        //Starts the pipeline to transofmr a single generated mesh point
        List<FeatureDescriptor> transformPoint(float t, ref MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline);
    }
}