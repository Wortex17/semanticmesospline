﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SMS.MeshGeneration;

namespace SMS.Features
{
    public interface FeaturePoint 
    {
        float T
        {
            get;
        }

        /// <summary>
        /// Should list all descriptors on this point, ordered explicit sorting.
        /// </summary>
        /// <returns></returns>
        FeatureDescriptor[] getDescriptors();

        List<FeatureDescriptor> getDecriptorsInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline);
        List<FeatureDescriptor> transformPoint(float t, ref MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline);
    }
}