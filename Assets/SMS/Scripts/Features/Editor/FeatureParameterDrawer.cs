﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace SMS.Features
{
    
    [CustomPropertyDrawer(typeof(FeatureParameter), true)]
    public class FeatureParameterDrawer : PropertyDrawer
    {
        GUIContent ConstantValueLabel = new GUIContent("Value", "Constant floating point number");
        GUIContent VariableValueLabel = new GUIContent("Param", "Context parameter name");
        GUIContent MappingLabel = new GUIContent("Mapping", "Map Context [0,1] to Custom Range");
        GUIContent TypeLabel = new GUIContent("Type", "How the Value should be applied. \n TIME: Spline time range\n NORMDIST: Normalized distance along the spline");

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 4f;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            Rect fieldRect = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Calculate rects
            Rect modeRect = new Rect(fieldRect.x, fieldRect.y, fieldRect.width, fieldRect.height * 0.25f);
            Rect l2Rect = new Rect(position.x + position.width * 0.1f, fieldRect.y + fieldRect.height * 0.25f, position.width * 0.9f, fieldRect.height * 0.25f);
            Rect l3Rect = new Rect(position.x + position.width * 0.1f, fieldRect.y + fieldRect.height * 0.5f, position.width * 0.9f, fieldRect.height * 0.25f);
            Rect l4Rect = new Rect(position.x + position.width * 0.1f, fieldRect.y + fieldRect.height * 0.75f, position.width * 0.9f, fieldRect.height * 0.25f);

            EditorGUI.PropertyField(modeRect, property.FindPropertyRelative("mMode"), GUIContent.none);
            if (property.FindPropertyRelative("mMode").enumValueIndex == (int)FeatureParameter.InputMode.CONSTANT)
            {
                EditorGUI.PropertyField(l2Rect, property.FindPropertyRelative("mConstantValue"), ConstantValueLabel);
                EditorGUI.PropertyField(l3Rect, property.FindPropertyRelative("mType"), TypeLabel);
            }
            else
            {
                EditorGUI.PropertyField(l2Rect, property.FindPropertyRelative("mParameterName"), VariableValueLabel);
                EditorGUI.PropertyField(l3Rect, property.FindPropertyRelative("mParameterMapping"), MappingLabel);
                EditorGUI.PropertyField(l4Rect, property.FindPropertyRelative("mType"), TypeLabel);
            }

            /*
            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("mConstantValue"), GUIContent.none);
            EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("mParameterName"), GUIContent.none);
            EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("mMode"), GUIContent.none);
            */

            EditorGUI.EndProperty();
        }
    }
}