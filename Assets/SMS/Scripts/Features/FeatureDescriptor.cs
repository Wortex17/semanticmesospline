﻿using UnityEngine;
using System.Collections;

using SMS.MeshGeneration;

namespace SMS.Features
{
    /// <summary>
    /// A Feature Desciptor is a baseclass for a Deformer/Transformer of the BaseMesh
    /// </summary>
    public interface FeatureDescriptor
    {
        FeaturePoint FP
        {
            get;
        }


        bool isInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline);
        void transformPoint(float t, ref MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline);
    }
}