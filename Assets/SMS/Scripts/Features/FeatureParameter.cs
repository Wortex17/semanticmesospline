﻿using UnityEngine;
using System.Collections;

namespace SMS.Features
{
    /// <summary>
    /// Describes a normalized floating point Input Parameter of any Feature Descriptor.
    /// </summary>
    [System.Serializable]
    public class FeatureParameter
    {

        public FeatureParameter(float constant)
        {
            this.ConstantValue = constant;
        }

        public enum InputMode
        {
            CONSTANT = 0,
            VARIABLE = 1
        }

        public enum OutputType
        {
            TIME = 0, //Time [0,1] 
            NORMDIST = 1 //Normalized Distance [0,1]
        }

        public InputMode Mode
        {
            get
            {
                return mMode;
            }
            set
            {
                mMode = value;
            }
        }
        public OutputType Type
        {
            get
            {
                return mType;
            }
            set
            {
                mType = value;
            }
        }

        public float ConstantValue
        {
            get
            {
                return mConstantValue;
            }
            set
            {
                mConstantValue = value;
            }
        }

        public string ParameterName
        {
            get
            {
                return mParameterName;
            }
            set
            {
                mParameterName = value;
            }
        }
        public AnimationCurve ParameterMapping
        {
            get
            {
                return mParameterMapping;
            }
            set
            {
                mParameterMapping = value;
            }
        }

        /// <summary>
        /// Returns ConstantValue or evaluated Parameter, depending on the Mode
        /// </summary>
        public float getValue(Transform at)
        {
            switch (mMode)
            {
                case InputMode.CONSTANT:
                    return ConstantValue;
                case InputMode.VARIABLE:
                    return evaluateVariableValue(at);
                default:
                    return 0f;
            }
        }
        /// <summary>
        /// Returns ConstantValue or evaluated Parameter, depending on the Mode
        /// </summary>
        public float getValue(MonoBehaviour at)
        {
            return getValue(at.transform);
        }
        /// <summary>
        /// Returns ConstantValue or evaluated Parameter, depending on the Mode
        /// </summary>
        public float getValue(GameObject at)
        {
            return getValue(at.transform);
        }

        public float evaluateVariableValue(Transform at)
        {
            float paramValue = Semantics.ContextLayer.getModifiedContextValueAt(at, mParameterName);
            return mParameterMapping.Evaluate(paramValue);
        }
        public float evaluateVariableValue(MonoBehaviour at)
        {
            return evaluateVariableValue(at.transform);
        }
        public float evaluateVariableValue(GameObject at)
        {
            return evaluateVariableValue(at.transform);
        }

        //Temporary hack for nicer values [0,1] (not restricted by concept, so a multitude of constant ranges should be possible)
        [SerializeField, Range(0,1)]
        float mConstantValue = 1f;
        [SerializeField]
        string mParameterName = "";
        [SerializeField]
        InputMode mMode = InputMode.CONSTANT;
        [SerializeField]
        OutputType mType = OutputType.TIME;
        [SerializeField]
        AnimationCurve mParameterMapping = AnimationCurve.Linear(0f, 0f, 1f, 1f);
    }
}