﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;
using SMS.Behaviours;
using SMS.Features;

namespace SMS.MeshGeneration
{
    public static class MeshGenerator
    {
        const float HULL_RADIUS_FACTOR = 0.1f;
        public class MeshData
        {
            public List<Vector3> Vertices = new List<Vector3>();
            public List<Vector3> Normals = new List<Vector3>();
            public List<Vector2> UVs = new List<Vector2>();
            //Containes the Vertex indices that form triangles
            public List<int> Triangles = new List<int>();

            //Time needed to generate
            public float TimeNeeded = 0f;

            public MeshData assignToMesh(Mesh mesh)
            {
                mesh.vertices = Vertices.ToArray();
                mesh.normals = Normals.ToArray();
                mesh.uv = UVs.ToArray();
                mesh.triangles = Triangles.ToArray();

                return this;
            }
        }

        public static MeshData generateEmpty()
        {
            MeshData data = new MeshData();
            return data;
        }

        public static MeshData extendMesh(MeshData mesh, Transform origin, CubicHermiteSpline spline, FeatureContainer featureContainer, int sampleResolution = 100, int hullResolution = 100)
        {
            float startGenerationTime = Time.realtimeSinceStartup;

            mesh.Vertices.Capacity = mesh.Vertices.Count + sampleResolution * hullResolution;
            mesh.Normals.Capacity = mesh.Normals.Count + sampleResolution * hullResolution;
            mesh.UVs.Capacity = mesh.UVs.Count + sampleResolution * hullResolution;
            mesh.Triangles.Capacity = mesh.Triangles.Count + sampleResolution * hullResolution * 2;

            // The starting index for the vertices.
            int vertStartIdx = mesh.Vertices.Count;

            float[] sampleSteps = CubicHermiteSpline.getSampleSteps((uint)Mathf.Max(0, sampleResolution), true);
            Vector3[] samplePositions = spline.getSamplePositions((uint)Mathf.Max(0, sampleResolution));
            float[] sampleRadii = spline.getSampleRadii((uint)Mathf.Max(0, sampleResolution));
            Vector3[][] sampleHull = ParallelTransport.getHullNormals(spline, sampleSteps, (uint)hullResolution);

            for (int i = 0; i < sampleHull.Length; i++)
            {
                float t = sampleSteps[i];
                Vector3 basePoint = samplePositions[i];
                float baseRadius = sampleRadii[i] * spline.BaseSize * HULL_RADIUS_FACTOR;
                Vector3[] hullRingNormals = sampleHull[i];

                int n = 0;
                foreach (Vector3 normal in hullRingNormals)
                {

                    //Create Point descriptor
                    MeshPointDescriptor point = new MeshPointDescriptor();
                    point.MesoPosition = basePoint;
                    point.Normal = normal;
                    point.setPosition(baseRadius);
                    point.HullDirection = (float)n / (float)hullRingNormals.Length;
                    //BaseForm Complete


                    if (featureContainer != null)
                    {
                        MeshPointDescriptor originPoint = point;
                        //Apply Feature Transformation
                        featureContainer.transformPoint(t, ref point, originPoint, spline);
                    }

                    //Convert Point Desciptor to Vertex / Add Point to Mesh
                    Vector3 positionRelativeToCustomOrigin = origin.InverseTransformPoint(spline.transform.TransformPoint(point.Position));
                    Vector3 normalRelativeToCustomOrigin = origin.InverseTransformDirection(spline.transform.TransformDirection(point.Normal));
                    mesh.Vertices.Add(positionRelativeToCustomOrigin);
                    mesh.Normals.Add(normalRelativeToCustomOrigin);
                    mesh.UVs.Add(new Vector2(
                        ((float)n) / hullRingNormals.Length,
                        ((float)i) / sampleHull.Length
                        ));

                    n++;
                }

                if (i == 0 && vertStartIdx == 0)
                {
                    continue;
                }
                
                int prevRingIdx = (i - 1) * hullResolution;
                int currentRingIdx = i * hullResolution;

                ///WARNING:
                ///Stitching while extending, as implemented here,
                ///will only work if both meshes have the same sample & hull resolution
                /// Stitching is the case when i == 0 && vertStartIdx > 0

                for (int h = 0; h < hullResolution; h++)
                {
                    int hn = h + 1;
                    if (hn == hullResolution)
                        hn = 0;

                    mesh.Triangles.Add(vertStartIdx + prevRingIdx + h);
                    mesh.Triangles.Add(vertStartIdx + prevRingIdx + hn);
                    mesh.Triangles.Add(vertStartIdx + currentRingIdx + h);

                    mesh.Triangles.Add(vertStartIdx + prevRingIdx + hn);
                    mesh.Triangles.Add(vertStartIdx + currentRingIdx + hn);
                    mesh.Triangles.Add(vertStartIdx + currentRingIdx + h);
                }
            }

            mesh.TimeNeeded += Time.realtimeSinceStartup - startGenerationTime;
            return mesh;
        }

        public static MeshData generateMesh(CubicHermiteSpline spline, Transform origin, FeatureContainer featureContainer, int sampleResolution = 100, int hullResolution = 100)
        {
            return extendMesh(generateEmpty(), origin, spline, featureContainer, sampleResolution, hullResolution);
        }

        //Used for e.g. flat caps
        public static MeshData generateFilledCrossSection(MeshData mesh, Transform origin, CubicHermiteSpline spline, float t, bool backwards, FeatureContainer featureContainer, int hullResolution = 100)
        {
            float startGenerationTime = Time.realtimeSinceStartup;


            Vector3 basePoint = spline.getPositionAt(t);
            float baseRadius = spline.getRadiusAt(t) * spline.BaseSize * HULL_RADIUS_FACTOR;
            Vector3[] hullRingNormals = ParallelTransport.getHullNormalsAt(spline, t, (uint)hullResolution);

            int vertStartIdx = mesh.Vertices.Count;

            int n = 0;
            foreach (Vector3 normal in hullRingNormals)
            {
                //Create Point descriptor
                MeshPointDescriptor point = new MeshPointDescriptor();
                point.MesoPosition = basePoint;
                point.Normal = normal;
                point.setPosition(baseRadius);
                point.HullDirection = (float)n / (float)hullRingNormals.Length;
                //BaseForm Complete


                if (featureContainer != null)
                {
                    MeshPointDescriptor originPoint = point;
                    //Apply Feature Transformation
                    featureContainer.transformPoint(t, ref point, originPoint, spline);
                }

                //Convert Point Desciptor to Vertex / Add Point to Mesh
                Vector3 positionRelativeToCustomOrigin = origin.InverseTransformPoint(spline.transform.TransformPoint(point.Position));
                Vector3 normalRelativeToCustomOrigin = origin.InverseTransformDirection(spline.transform.TransformDirection(point.Normal));
                mesh.Vertices.Add(positionRelativeToCustomOrigin);
                mesh.Normals.Add(normalRelativeToCustomOrigin);
                mesh.UVs.Add(new Vector2(
                    ((float)n) / hullRingNormals.Length,
                    0f
                    ));

                n++;
            }
            mesh.Vertices.Add(basePoint);
            mesh.Normals.Add(ParallelTransport.getTangentAt(spline, t) * ((backwards) ? -1f : 1f));
            mesh.UVs.Add(new Vector2(0, 0));

            for (int h = 0; h < hullResolution; h++)
            {
                int hn = h + 1;
                if (hn == hullResolution)
                    hn = 0;

                mesh.Triangles.Add(vertStartIdx + h); //Hull1
                if (backwards)
                {
                    mesh.Triangles.Add(vertStartIdx + hullResolution); //MidPoint
                }
                mesh.Triangles.Add(vertStartIdx + hn); //Hull2
                if (!backwards)
                {
                    mesh.Triangles.Add(vertStartIdx + hullResolution); //MidPoint
                }
            }


            mesh.TimeNeeded += Time.realtimeSinceStartup - startGenerationTime;
            return mesh;
        }

        public static MeshData generatePointy(MeshData mesh, Transform origin, CubicHermiteSpline spline, float t_from, float t_to, bool backwards, FeatureContainer featureContainer, int hullResolution = 100)
        {
            float startGenerationTime = Time.realtimeSinceStartup;

            Vector3 baseStartPoint = spline.getPositionAt(t_from);
            float baseStartRadius = spline.getRadiusAt(t_from) * spline.BaseSize * HULL_RADIUS_FACTOR * 1.2f;
            Vector3[] startHullRingNormals = ParallelTransport.getHullNormalsAt(spline, t_from, (uint)hullResolution);


            Vector3 baseEndPoint = spline.getPositionAt(t_to);

            int vertStartIdx = mesh.Vertices.Count;

            int n = 0;
            foreach (Vector3 normal in startHullRingNormals)
            {
                //Create Point descriptor
                MeshPointDescriptor point = new MeshPointDescriptor();
                point.MesoPosition = baseStartPoint;
                point.Normal = normal;
                point.setPosition(baseStartRadius);
                point.HullDirection = (float)n / (float)startHullRingNormals.Length;
                //BaseForm Complete


                if (featureContainer != null)
                {
                    MeshPointDescriptor originPoint = point;
                    //Apply Feature Transformation
                    featureContainer.transformPoint(t_from, ref point, originPoint, spline);
                }

                //Convert Point Desciptor to Vertex / Add Point to Mesh
                Vector3 positionRelativeToCustomOrigin = origin.InverseTransformPoint(spline.transform.TransformPoint(point.Position));
                Vector3 normalRelativeToCustomOrigin = origin.InverseTransformDirection(spline.transform.TransformDirection(point.Normal));
                mesh.Vertices.Add(positionRelativeToCustomOrigin);
                mesh.Normals.Add(normalRelativeToCustomOrigin);
                mesh.UVs.Add(new Vector2(
                    ((float)n) / startHullRingNormals.Length,
                    0f
                    ));

                n++;
            }

            mesh.Vertices.Add(baseEndPoint);
            mesh.Normals.Add(ParallelTransport.getTangentAt(spline, t_to) * ((backwards) ? -1f : 1f));
            mesh.UVs.Add(new Vector2(0, 0));

            for (int h = 0; h < hullResolution; h++)
            {
                int hn = h + 1;
                if (hn == hullResolution)
                    hn = 0;

                mesh.Triangles.Add(vertStartIdx + h); //Hull1
                if (backwards)
                {
                    mesh.Triangles.Add(vertStartIdx + hullResolution); //MidPoint
                }
                mesh.Triangles.Add(vertStartIdx + hn); //Hull2
                if (!backwards)
                {
                    mesh.Triangles.Add(vertStartIdx + hullResolution); //MidPoint
                }
            }

            mesh.TimeNeeded += Time.realtimeSinceStartup - startGenerationTime;
            return mesh;
        }

        public static MeshData generateDome(MeshData mesh, Transform origin, CubicHermiteSpline spline, float t_from, bool backwards, FeatureContainer featureContainer, int vResolution = 5, int hullResolution = 100)
        {
            float startGenerationTime = Time.realtimeSinceStartup;

            Vector3 baseStartPoint = spline.getPositionAt(t_from);
            float baseStartRadius = spline.getRadiusAt(t_from) * spline.BaseSize * HULL_RADIUS_FACTOR;
            Vector3[] startHullRingNormals = ParallelTransport.getHullNormalsAt(spline, t_from, (uint)hullResolution);

            MeshPointDescriptor[] startHullRingPoints = new MeshPointDescriptor[startHullRingNormals.Length];

            //Vector3 baseEndPoint = spline.getPositionAt(t_to);

            int vertStartIdx = mesh.Vertices.Count;


            //First we create the starting ring and collect their point descriptors
            int n = 0;
            foreach (Vector3 normal in startHullRingNormals)
            {
                //Create Point descriptor
                MeshPointDescriptor point = new MeshPointDescriptor();
                point.MesoPosition = baseStartPoint;
                point.Normal = normal;
                point.setPosition(baseStartRadius);
                point.HullDirection = (float)n / (float)startHullRingNormals.Length;
                //BaseForm Complete

                if (featureContainer != null)
                {
                    MeshPointDescriptor originPoint = point;
                    //Apply Feature Transformation
                    featureContainer.transformPoint(t_from, ref point, originPoint, spline);
                }

                startHullRingPoints[n] = point;
                n++;
            }

            //Generate the point descriptor for the final peak point
            MeshPointDescriptor peakPoint = new MeshPointDescriptor();
            peakPoint.MesoPosition = baseStartPoint;
            peakPoint.Normal = (ParallelTransport.getTangentAt(spline, t_from) * ((backwards) ? -1f : 1f)).normalized;
            peakPoint.setPosition(baseStartRadius);


            //Now insert the new points that form the dome

            //Amount of inserted ring normals between the starting ring(included) and the final point normal(exluded).
            int domeRingCount = 1 + Mathf.CeilToInt((float)vResolution/2.0f);
            float hemiDomeStepsize = 1.0f / (float)domeRingCount;

            // [ring][normal]
            MeshPointDescriptor[][] hullPoints = new MeshPointDescriptor[domeRingCount][];

            for (int v = 0; v < domeRingCount; v++)
            {
                hullPoints[v] = new MeshPointDescriptor[startHullRingNormals.Length];
                for (int u = 0; u < startHullRingNormals.Length; u++)
                {
                    float currentDistance = hemiDomeStepsize * (float)v;
                    MeshPointDescriptor insertPoint = startHullRingPoints[u];

                    insertPoint.Normal = Vector3.Lerp(insertPoint.Normal, peakPoint.Normal, currentDistance).normalized;
                    insertPoint.setPosition(Mathf.Lerp(insertPoint.MesoDistance, peakPoint.MesoDistance, currentDistance));

                    hullPoints[v][u] = insertPoint;
                }
            }

            //Insert generated points into the mesh
            for (int v = 0; v < domeRingCount; v++)
            {
                for (int u = 0; u < startHullRingNormals.Length; u++)
                {
                    MeshPointDescriptor point = hullPoints[v][u];
                    //Convert Point Desciptor to Vertex / Add Point to Mesh
                    Vector3 positionRelativeToCustomOrigin = origin.InverseTransformPoint(spline.transform.TransformPoint(point.Position));
                    Vector3 normalRelativeToCustomOrigin = origin.InverseTransformDirection(spline.transform.TransformDirection(point.Normal));
                    mesh.Vertices.Add(positionRelativeToCustomOrigin);
                    mesh.Normals.Add(normalRelativeToCustomOrigin);

                    mesh.UVs.Add(new Vector2(
                        ((float)u) / startHullRingNormals.Length,
                        ((float)v) / domeRingCount
                        ));
                }
            }

            for (int v = 1; v < hullPoints.Length; v++)
            {
                int prevRingIndex = (v - 1) * startHullRingNormals.Length;
                int ringIndex = v * startHullRingNormals.Length;

                for (int u = 0; u < startHullRingNormals.Length; u++)
                {
                    int pointIndex = u;
                    int nextPointIndex = u+1;
                    if (nextPointIndex == startHullRingNormals.Length)
                    {
                        nextPointIndex = 0;
                    }

                    mesh.Triangles.Add(vertStartIdx + prevRingIndex + pointIndex);
                    if (backwards)
                    {
                        mesh.Triangles.Add(vertStartIdx + ringIndex + pointIndex);
                    }
                    mesh.Triangles.Add(vertStartIdx + ringIndex + nextPointIndex);
                    if (!backwards)
                    {
                        mesh.Triangles.Add(vertStartIdx + ringIndex + pointIndex);
                    }

                    mesh.Triangles.Add(vertStartIdx + prevRingIndex + pointIndex);
                    if (backwards)
                    {
                        mesh.Triangles.Add(vertStartIdx + ringIndex + nextPointIndex);
                    }
                    mesh.Triangles.Add(vertStartIdx + prevRingIndex + nextPointIndex);
                    if (!backwards)
                    {
                        mesh.Triangles.Add(vertStartIdx + ringIndex + nextPointIndex);
                    }
                }
            }


            //Add the peakpoint to the mesh
            mesh.Vertices.Add(origin.InverseTransformPoint(spline.transform.TransformPoint(peakPoint.Position)));
            mesh.Normals.Add(origin.InverseTransformDirection(spline.transform.TransformDirection(peakPoint.Normal)));
            mesh.UVs.Add(Vector2.one);

            int lastRingIndex = (hullPoints.Length - 1) * startHullRingNormals.Length;
            int peakPointAbsoluteIndex = mesh.Vertices.Count - 1;
            for (int u = 0; u < startHullRingNormals.Length; u++)
            {
                int pointIndex = u;
                int nextPointIndex = u + 1;
                if (nextPointIndex == startHullRingNormals.Length)
                {
                    nextPointIndex = 0;
                }

                mesh.Triangles.Add(vertStartIdx + lastRingIndex + pointIndex);
                if (backwards)
                {
                    mesh.Triangles.Add(peakPointAbsoluteIndex);
                }
                mesh.Triangles.Add(vertStartIdx + lastRingIndex + nextPointIndex);
                if (!backwards)
                {
                    mesh.Triangles.Add(peakPointAbsoluteIndex);
                }
            }


            mesh.TimeNeeded += Time.realtimeSinceStartup - startGenerationTime;
            return mesh;
        }

        //Add a flat (unomptimized) cap
        public static MeshData generateMeshStartCap(MeshData mesh, Transform origin, CubicHermiteSpline spline, FeatureContainer featureContainer, int sampleResolution = 100, int hullResolution = 100)
        {
            return generateDome(mesh, origin, spline, 0f, true, featureContainer, hullResolution);
        }

        public static MeshData generateMeshEndCap(MeshData mesh, Transform origin, CubicHermiteSpline spline, FeatureContainer featureContainer, int sampleResolution = 100, int hullResolution = 100)
        {
            return generateDome(mesh, origin, spline, 1.0f, false, featureContainer, sampleResolution, hullResolution);
        }
    }
}