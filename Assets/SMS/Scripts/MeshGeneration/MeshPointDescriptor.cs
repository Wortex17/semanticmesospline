﻿using UnityEngine;
using System.Collections;

namespace SMS.MeshGeneration
{
    /// <summary>
    /// A shared structure taht desribes a point that is generated from the meso spline
    /// This is never stored, so we dont need to serialize
    /// </summary>
    public struct MeshPointDescriptor
    {
        public Vector3 Position;
        public Vector3 MesoPosition;
        public Vector3 Normal;

        public Vector3 setPosition(float radius)
        {
            Position = MesoPosition + Normal * radius;
            return Position;
        }

        /// <summary>
        /// Returns the distance from MesoPosition.
        /// This MAY be the radius, depending on the changes features are making
        /// directly to the Position.
        /// </summary>
        public float MesoDistance
        {
            get
            {
                return (Position - MesoPosition).magnitude;
            }
        }

        //The relational direction on the unit circle in which this point was generated
        public float HullDirection;

    }
}