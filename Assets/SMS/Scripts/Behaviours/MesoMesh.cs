﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;
using SMS.MeshGeneration;

namespace SMS.Behaviours
{
    /// <summary>
    /// Behaviour that takes a MesoPSline and generates a Mesh from it
    /// </summary>
    [ExecuteInEditMode, RequireComponent(typeof(MeshFilter))]
    public class MesoMesh : MonoBehaviour
    {

        public Mesh GeneratedMesh = null;
        public MesoSpline StartSpline = null;

        public bool AutoGenerate = true;
        [Range(2, 1000)]
        public int SamplingResolution = 100;
        [Range(4, 1000)]
        public int HullResolution = 50;

        [Tooltip("Generate a Mesh for the StartSpline and all next splines")]
        public bool GenerateFullMesh = true;
        public bool GenerateFeatureTransform = true;
        [Tooltip("Optimize Mesh after creating, using Unitys built-in optimizer")]
        public bool OptimizeMesh = true;

        public float TimeNeeded = 0f;

        public void forceMeshUpdate()
        {
            updateMesh();
        }

        #region Protected Methods

        protected void updateMesh()
        {

            if (GeneratedMesh == null)
                GeneratedMesh = new Mesh();
            this.GetComponent<MeshFilter>().sharedMesh = GeneratedMesh;

            GeneratedMesh.Clear();
            if (StartSpline == null || StartSpline.ControlPoints.Count < 2)
            {
                GeneratedMesh.name = "empty";
                return;
            }
            GeneratedMesh.name = StartSpline.gameObject.name;

            GeneratedMesh.Clear();
            
            if(OptimizeMesh)
                GeneratedMesh.Optimize();

            MeshGenerator.MeshData currentMesh = MeshGenerator.generateMesh(StartSpline,
                    this.transform,
                    (GenerateFeatureTransform) ? StartSpline.GetComponent<MesoSplineFeatures>() : null,
                    SamplingResolution,
                    HullResolution
                    );

            if (GenerateFullMesh)
            {
                int n = 1;
                bool hasEndCap = false;
                foreach (MesoSpline pivotSpline in StartSpline.getNextSplines(false))
                {
                    MeshGenerator.extendMesh(
                        currentMesh,
                        this.transform,
                        pivotSpline,
                        (GenerateFeatureTransform) ? pivotSpline.GetComponent<MesoSplineFeatures>() : null,
                        SamplingResolution,
                        HullResolution
                        );
                    if (pivotSpline.NextSpline == null)
                    {
                        hasEndCap = true;
                        MeshGenerator.generateMeshEndCap(
                            currentMesh,
                            this.transform,
                            pivotSpline,
                            (GenerateFeatureTransform) ? pivotSpline.GetComponent<MesoSplineFeatures>() : null,
                            SamplingResolution,
                            HullResolution
                        );
                    }
                    n++;
                }

                if (!hasEndCap)
                {
                    hasEndCap = true;
                    MeshGenerator.generateMeshEndCap(
                        currentMesh,
                        this.transform,
                        StartSpline,
                        (GenerateFeatureTransform) ? StartSpline.GetComponent<MesoSplineFeatures>() : null,
                        SamplingResolution,
                        HullResolution
                    );
                }
            }

            MeshGenerator.generateMeshStartCap(
                currentMesh,
                this.transform,
                StartSpline,
                (GenerateFeatureTransform) ? StartSpline.GetComponent<MesoSplineFeatures>() : null,
                SamplingResolution,
                HullResolution
            );


            TimeNeeded = currentMesh.assignToMesh(GeneratedMesh).TimeNeeded;
            
        }

        #endregion

        void onSplineChanged(MesoSpline spline)
        {
            if (spline == this.StartSpline)
            {
                if (AutoGenerate && this.enabled)
                {
                    this.updateMesh();
                }
            }
            else
            {
                spline.OnChanged -= this.onSplineChanged;
            }
        }

        #region Unity Messages

        // Use this for initialization
        void Start()
        {

        }

        void Update()
        {
            //this.updateMesh();
        }

        void OnValidate()
        {
            if (StartSpline != null)
            {
                StartSpline.OnChanged -= this.onSplineChanged;
                StartSpline.OnChanged += this.onSplineChanged;
            }
        }

        #endregion
    }
}