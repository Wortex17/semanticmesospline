﻿using UnityEngine;
using System.Collections;

using SMS.Behaviours;
using FeatureParameter = SMS.Features.FeatureParameter;
using SMS.MeshGeneration;

namespace SMS.Behaviours.Features.Core
{
    public class ForkFeature : SMS.Behaviours.MesoFeature, SMS.Behaviours.ForkInterface
    {
        public FeatureParameter Size
        {
            get
            {
                return mSize;
            }
        }

        public FeatureParameter Orientation
        {
            get
            {
                return mOrientation;
            }
        }
        public FeatureParameter Tilt
        {
            get
            {
                return mTilt;
            }
        }

        public MesoSpline AttachedSpline
        {
            get
            {
                return mAttachedSpline;
            }
            set
            {
                if (mAttachedSpline != value)
                {
                    if (mAttachedSpline != null)
                    {
                        mAttachedSpline.ParentForkFeature = null;
                    }
                    mAttachedSpline = value;
                    if (mAttachedSpline != null)
                    {
                        mAttachedSpline.ParentForkFeature = this;
                    }
                }
            }
        }

        public MesoSpline ParentSpline 
        {
            get
            {
                return MesoFP.BoundSpline;
            }
        }

        public float T
        {
            get
            {
                return MesoFP.T;
            }
        }

        public float getSizeForForkedSpline()
        {
            return Size.getValue(this);
        }

        public Vector3 getPositionForForkedSpline()
        {
            return this.transform.position;
        }

        public Quaternion getRotationForForkedSpline()
        {
            if (ParentSpline == null)
                return Quaternion.identity;

            MesoSpline boundSpline = ParentSpline;

            Vector3 normal = Math.ParallelTransport.getNormalAt(boundSpline, T);

            //Get the tangent of the fork point
            Vector3 tangent = boundSpline.get1stDerivativeAt(T).normalized;

            // Gives us the sway along a horizontal disk, centered and aligned to the tangent
            float horizontalOrientation = Mathf.Repeat(Orientation.getValue(this.gameObject), 1f);
            //Rotate tangent by horizontalOrientation around normal
            tangent = Quaternion.AngleAxis(horizontalOrientation * 360f, normal) * tangent;

            // Gives us the sway along a vertical disk, centered and aligned to the normal
            float verticalOrientation = Mathf.Repeat(Tilt.getValue(this.gameObject), 1f);
            Vector3 binormal = Vector3.Cross(normal, tangent);
            //Rotate tangent by verticalOrientation around binormal
            tangent = Quaternion.AngleAxis(verticalOrientation * 360f, binormal) * tangent;


            //Transform the tangent into world space
            Vector3 tangentInWorld = boundSpline.transform.TransformDirection(tangent);

            //Use The worlds right/X axis be our reference axis
            Quaternion rotation = Quaternion.FromToRotation(Vector3.right, tangentInWorld);



            return rotation;
        }


        public override void transformPoint(float t, ref MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
        }

        public override bool isInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
            return mSize.getValue(this) != 0f;
        }

        [SerializeField]
        protected FeatureParameter mSize = new SMS.Features.FeatureParameter(0f);
        [SerializeField]
        protected FeatureParameter mOrientation = new SMS.Features.FeatureParameter(0f);
        [SerializeField]
        protected FeatureParameter mTilt = new SMS.Features.FeatureParameter(0f);
        [SerializeField, HideInInspector]
        protected MesoSpline mAttachedSpline = null;

        /*
        void OnDrawGizmos()
        {
            
            MesoSpline boundSpline = MesoFP.BoundSpline;

            //Get the tangent of the fork point
            Vector3 tangent = boundSpline.get1stDerivativeAt(MesoFP.T).normalized;
            Vector3 tangentInWorld = boundSpline.transform.TransformDirection(tangent);

            Vector3 normal = Math.ParallelTransport.getNormalAt(boundSpline, MesoFP.T); ;

            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + tangentInWorld);
            Gizmos.color = Color.black;
            Gizmos.DrawLine(transform.position, transform.position + normal);

            float horizontalOrientation = Mathf.Repeat(Orientation.getValue(this.gameObject), 1f);

            tangent = Quaternion.AngleAxis(horizontalOrientation * 360f, normal) * tangent;
            tangentInWorld = boundSpline.transform.TransformDirection(tangent);

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + tangentInWorld);
            
        }
         * */

    }
}