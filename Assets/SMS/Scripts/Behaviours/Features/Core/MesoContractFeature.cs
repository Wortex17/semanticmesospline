﻿using UnityEngine;
using System.Collections;

using SMS.Behaviours;
using FeatureParameter = SMS.Features.FeatureParameter;
using SMS.MeshGeneration;

namespace SMS.Behaviours.Features.Core
{
    public class MesoContractFeature : SMS.Behaviours.Features.Base.RangedFeature
    {
        public FeatureParameter Strength
        {
            get
            {
                return mStrength;
            }
        }
        public AnimationCurve Stamp
        {
            get
            {
                return mStamp;
            }
        }

        public override void transformPoint(float t, ref MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
            //get the original thickness, to make our effects relative to it.
            float effectStr = originPoint.MesoDistance;

            float radius = transformedPoint.MesoDistance;

            float distance = 0f;
            float maxDistance = Range.getValue(this);
            if (maxDistance  > 0)
            {
                // Calculate nromalized distance from featurepoint, so we can apply the stamp curve
                distance = (t - FP.T) / maxDistance;
            }
            //Normalize the distance to a [0f,1f] range, to use it for the stamp
            distance = 0.5f + distance * 0.5f;
            radius -= effectStr * Strength.getValue(this) * Stamp.Evaluate(distance);

            transformedPoint.setPosition(radius);

        }

        [SerializeField]
        protected FeatureParameter mStrength = new SMS.Features.FeatureParameter(0f);
        [SerializeField]
        protected AnimationCurve mStamp = AnimationCurve.Linear(0f, 1f, 1f, 1f);

    }
}