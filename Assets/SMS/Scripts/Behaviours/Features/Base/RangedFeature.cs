﻿using UnityEngine;
using System.Collections;

using SMS.Behaviours;
using FeatureParameter = SMS.Features.FeatureParameter;
using SMS.MeshGeneration;

namespace SMS.Behaviours.Features.Base
{
    public abstract class RangedFeature : MesoFeature
    {
        public FeatureParameter Range
        {
            get
            {
                return mRange;
            }
        }

        public override bool isInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
            return isInCharge(this, this.Range, t);
        }

        public static bool isInCharge(MesoFeature feature, FeatureParameter range, float t)
        {
            return (Mathf.Abs(t - feature.FP.T) <= range.getValue(feature));
        }

        [SerializeField]
        protected FeatureParameter mRange = new SMS.Features.FeatureParameter(0f);

    }
}