﻿using UnityEngine;
using System.Collections;

using SMS.Behaviours;
using FeatureParameter = SMS.Features.FeatureParameter;
using SMS.MeshGeneration;

namespace SMS.Behaviours.Features.Base
{
    public abstract class OrientedFeature : MesoFeature
    {
        public FeatureParameter Circumference
        {
            get
            {
                return mCircumference;
            }
        }

        public FeatureParameter Orientation
        {
            get
            {
                return mOrientation;
            }
        }

        public override bool isInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
            return isInCharge(this, this.Circumference, this.Orientation, transformedPoint);
        }

        public static bool isInCharge(MesoFeature feature, FeatureParameter circumference, FeatureParameter orientation, MeshPointDescriptor transformedPoint)
        {
            return (getHullDistance(transformedPoint, orientation.getValue(feature)) <= circumference.getValue(feature));
        }

        public static float getHullDistance(MeshPointDescriptor point, float Orientation)
        {
            float m = point.HullDirection;
            float a = Orientation - 1f;
            float b = Orientation;
            float c = Orientation + 1f;


            return Mathf.Min(Mathf.Abs(m - a), Mathf.Abs(m - b), Mathf.Abs(m - c));
        }

        [SerializeField]
        protected FeatureParameter mCircumference = new SMS.Features.FeatureParameter(0f);
        [SerializeField]
        protected FeatureParameter mOrientation = new SMS.Features.FeatureParameter(0f);

    }
}