﻿using UnityEngine;
using System.Collections;

using SMS.Behaviours;
using FeatureParameter = SMS.Features.FeatureParameter;
using SMS.MeshGeneration;

namespace SMS.Behaviours.Features.Base
{
    public abstract class RangedOrientedFeature : MesoFeature
    {
        public FeatureParameter Range
        {
            get
            {
                return mRange;
            }
        }
        public FeatureParameter Circumference
        {
            get
            {
                return mCircumference;
            }
        }

        public FeatureParameter Orientation
        {
            get
            {
                return mOrientation;
            }
        }

        public override bool isInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
            return isInCharge(this, this.Range, this.Circumference, this.Orientation, t, transformedPoint);
        }

        public static bool isInCharge(MesoFeature feature, FeatureParameter range, FeatureParameter circumference, FeatureParameter orientation, float t, MeshPointDescriptor transformedPoint)
        {
            return RangedFeature.isInCharge(feature, range, t) && OrientedFeature.isInCharge(feature, circumference, orientation, transformedPoint);
        }

        [SerializeField]
        protected FeatureParameter mRange = new SMS.Features.FeatureParameter(0f);
        [SerializeField]
        protected FeatureParameter mCircumference = new SMS.Features.FeatureParameter(0f);
        [SerializeField]
        protected FeatureParameter mOrientation = new SMS.Features.FeatureParameter(0f);

    }
}