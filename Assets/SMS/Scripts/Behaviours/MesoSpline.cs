﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;

namespace SMS.Behaviours
{
    /// <summary>
    /// Carrier of our chosen spline type.
    /// I implemented this with inheritance to be able to switch the inherited class and therefore
    /// the used spline type anytime
    /// </summary>
    [ExecuteInEditMode]
    public class MesoSpline : KochanekBartelsSpline
    {
        public override float BaseSize
        {
            get
            {
                if (PreviousSpline != null)
                {
                    return PreviousSpline.NextBaseSize;
                } else if(ParentFork != null)
                {
                    return ParentFork.getSizeForForkedSpline();
                }
                return 1.0f;
            }
        }

        public MesoSpline NextSpline
        {
            get
            {
                return mNextSpline;
            }
            set
            {
                if (mNextSpline != value && value != this)
                {
                    if (mNextSpline != null)
                    {
                        //Unlink
                        mNextSpline.mPreviousSpline = null;
                    }
                    mNextSpline = value;
                    if (mNextSpline != null)
                    {
                        //Link
                        mNextSpline.PreviousSpline = this;
                    }
                }
            }
        }
        /// <summary>
        /// The BaseSize handed down to the next spline
        /// </summary>
        public float NextBaseSize
        {
            get
            {
                return mNextBaseSize;
            }
            set
            {
                mNextBaseSize = Mathf.Max(0f, value);
            }
        }

        public MesoSpline PreviousSpline
        {
            get
            {
                return mPreviousSpline;
            }
            set
            {
                if (mPreviousSpline != value && value != this)
                {
                    if (ParentFork != null)
                    {
                        mParentForkFeature = null;
                    }
                    if (mPreviousSpline != null)
                    {
                        //Unlink
                        mPreviousSpline.mNextSpline = null;
                    }
                    mPreviousSpline = value;
                    if (mPreviousSpline != null)
                    {
                        //Link
                        mPreviousSpline.NextSpline = this;
                    }
                }
            }
        }

        public SMS.Behaviours.Features.Core.ForkFeature ParentForkFeature
        {
            set
            {
                //Unlink structural version first, if this isn't getting nulled/unlinked
                if (value != null)
                {
                    if (mParentForkPoint != null)
                    {
                        //Unlink
                        mParentForkPoint.AttachedSpline = null;
                        mParentForkPoint = null;
                    }
                }

                if (mParentForkFeature != value)
                {
                    var oldParentForkFeature = mParentForkFeature;
                    mParentForkFeature = value;
                    if (oldParentForkFeature != null)
                    {
                        //Unlink
                        oldParentForkFeature.AttachedSpline = null;
                    }
                    if (mParentForkFeature != null)
                    {
                        //Link
                        mParentForkFeature.AttachedSpline = this;
                    }
                    if (PreviousSpline != null)
                    {
                        PreviousSpline = null;
                    }
                }
            }
        }

        public ForkPoint ParentForkPoint
        {
            set
            {
                //Unlink structural version first, if this isn't getting nulled/unlinked
                if (value != null)
                {
                    if (mParentForkFeature != null)
                    {
                        //Unlink
                        mParentForkFeature.AttachedSpline = null;
                        mParentForkFeature = null;
                    }
                }

                if (mParentForkPoint != value)
                {
                    var oldParentForkPoint = mParentForkPoint;
                    mParentForkPoint = value;
                    if (oldParentForkPoint != null)
                    {
                        //Unlink
                        oldParentForkPoint.AttachedSpline = null;
                    }
                    if (mParentForkPoint != null)
                    {
                        //Link
                        mParentForkPoint.AttachedSpline = this;
                    }
                    if (PreviousSpline != null)
                    {
                        PreviousSpline = null;
                    }
                }
            }
        }

        public ForkInterface ParentFork
        {
            get
            {
                return (mParentForkPoint != null) ? (ForkInterface)mParentForkPoint : (ForkInterface)mParentForkFeature;
            }
        }

        public ControlPoint Start
        {
            get
            {
                return ControlPoints[0];
            }
            set
            {
                ControlPoints[0] = value;
            }
        }
        public ControlPoint End
        {
            get
            {
                return ControlPoints[ControlPoints.Count - 1];
            }
            set
            {
                ControlPoints[ControlPoints.Count - 1] = value;
            }
        }


        #region Public Fields

        [HideInInspector]
        public bool DrawControlPoints = true;
        [HideInInspector]
        public bool DrawControlLine = true;
        [HideInInspector]
        public bool DrawSpline = true;
        [Range(1, 5000)]
        public int DrawingResolution = 200;

        public bool DBG = false;

#if UNITY_EDITOR
        [HideInInspector, System.NonSerialized]
        public int HighlightIdx = -1;
#endif


        #endregion

        #region Public Methods


        public Vector3 getGlobalPositionAt(float t)
        {
            return this.transform.TransformPoint(getPositionAt(t));
        }

        //Returns all next MesoSplines from this one onwards (including this one)
        public List<MesoSpline> getNextSplines(bool includeThis = true)
        {
            List<MesoSpline> chain = new List<MesoSpline>();
            MesoSpline pivot = (includeThis) ? this : this.NextSpline;
            while (pivot != null)
            {
                chain.Add(pivot);
                pivot = pivot.NextSpline;
            }
            return chain;
        }

#if UNITY_EDITOR
        /// <summary>
        /// Mainly used in editor, adds a new control point
        /// </summary>
        /// <returns></returns>
        public Vector3 addControlPoint()
        {
            Vector3 newCP = Vector3.zero;
            if(ControlPoints.Count >= 2)
            {
                newCP = ControlPoints[ControlPoints.Count-1] + (ControlPoints[ControlPoints.Count-1].Position - ControlPoints[ControlPoints.Count-2].Position);
            }
            ControlPoints.Add(new ControlPoint(newCP));
            return newCP;
        }

        public delegate void OnChangedHandler(MesoSpline spline);
        public event OnChangedHandler OnChanged;

        public void notifyChanges()
        {
            updateApproximateLength();
            if (OnChanged != null)
            {
                OnChanged(this);
            }
        }

#endif


        #endregion


        #region Color Palette for Gizmos

        public static class ColorPalette
        {
            static public Color ControlPoint = new Color(0.6f, 0.6f, 1.0f);
            static public Color ControlPointHighlight = Color.yellow;
            static public Color ControlLine = new Color(0.6f, 0.6f, 1.0f, 0.75f);
            static public Color Spline = new Color(86f/255f, 210f/255f, 15f/255f, 0.9f);
        }

        #endregion

        #region Unity Messages

        void Update()
        {
            if (DBG)
            {
                DBG = false;
                notifyChanges();
            }

            if (ControlPoints.Count == 0)
            {
                ControlPoints.Add(new ControlPoint(Vector3.zero));
                ControlPoints.Add(new ControlPoint(Vector3.one));
                ControlPoints.Add(new ControlPoint(new Vector3(4f, 2f, 2f)));
                Debug.Log("Created new CMR ");
            }

            if (ControlPoints.Count > 0 && ControlPoints[0].Position != Vector3.zero)
            {
                ControlPoint start = ControlPoints[0];
                start.Position = Vector3.zero;
                ControlPoints[0] = start;
            }

            if (PreviousSpline != null)
            {
                Vector3 prevEnd = PreviousSpline.getGlobalPositionAt(1f);
                if (transform.position != prevEnd)
                {
                    transform.position = prevEnd;
                }
                /** * This Forces the radii to match. (Was used before the baseSize was impl.
                if (this.ControlPoints.Count > 0 && PreviousSpline.ControlPoints.Count > 0)
                {
                    ControlPoint start = this.Start;
                    ControlPoint end = PreviousSpline.End;

                    if (start.Radius != end.Radius)
                    {
                        start.Radius = end.Radius;
                        this.Start = start;
                    }
                }
                 * **/
            } else if (ParentFork != null)
            {
                Vector3 parentPos = ParentFork.getPositionForForkedSpline();
                if (transform.position != parentPos)
                {
                    transform.position = parentPos;
                }

                Quaternion forkRotation = ParentFork.getRotationForForkedSpline();
                if (transform.rotation != forkRotation)
                {
                    transform.rotation = forkRotation;
                }
            }
        }

        void OnDrawGizmos()
        {
            if (DrawControlPoints)
            {
                Gizmos.color = ColorPalette.ControlPoint;
                for (int i = 0; i < ControlPoints.Count; i++)
                {
                    ControlPoint cp = ControlPoints[i];
                    Gizmos.DrawSphere(transform.TransformPoint(cp), 0.025f);

                    if (i == HighlightIdx)
                    {
                        Gizmos.color = ColorPalette.ControlPointHighlight;
                        Gizmos.DrawWireSphere(transform.TransformPoint(cp), 0.05f);
                        Gizmos.color = ColorPalette.ControlPoint;
                    }
                }
            }

            if (DrawControlLine)
            {
                Gizmos.color = ColorPalette.ControlLine;
                for (int i = 1; i < ControlPoints.Count; i++)
                {
                    Gizmos.DrawLine(transform.TransformPoint(ControlPoints[i - 1]), transform.TransformPoint(ControlPoints[i]));
                }
            }

            if (DrawSpline)
            {
                Gizmos.color = ColorPalette.Spline;
                Vector3[] sample = getSamplePositions((uint)Mathf.Max(0, DrawingResolution), true);
                for (int s = 1; s < sample.Length; s++)
                {
                    Gizmos.DrawLine(transform.TransformPoint(sample[s - 1]), transform.TransformPoint(sample[s]));
                }
            }
        }

        #endregion


        [SerializeField, HideInInspector]
        MesoSpline mPreviousSpline = null;
        [SerializeField, HideInInspector]
        MesoSpline mNextSpline = null;
        [SerializeField, HideInInspector]
        float mNextBaseSize = 1f;
        /// <summary>
        /// Hack with two different fork fields to work around unitys serialization quirks
        /// (cannot serialize content of interface fields)
        /// </summary>
        [SerializeField, HideInInspector]
        Features.Core.ForkFeature mParentForkFeature = null;
        [SerializeField, HideInInspector]
        ForkPoint mParentForkPoint = null;
    }
}