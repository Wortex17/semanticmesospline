﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;

namespace SMS.Behaviours
{
    [CustomEditor(typeof(MesoFeature), true)]
    public class MesoFeatureEditor : Editor
    {

        MesoFeature fp;

        override public void OnInspectorGUI()
        {
            fp = (MesoFeature)this.target;
            if (fp == null)
                return;

            EditorGUI.BeginChangeCheck();

            fp.enabled = EditorGUILayout.Toggle("Enabled", fp.enabled);
            EditorGUILayout.Separator();

            DrawDefaultInspector();

            if (EditorGUI.EndChangeCheck() && fp.MesoFP != null && fp.MesoFP.BoundSpline != null)
            {
                fp.MesoFP.BoundSpline.notifyChanges();
            }
        }
    }
}