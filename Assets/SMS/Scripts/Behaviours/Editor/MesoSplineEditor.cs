﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;

namespace SMS.Behaviours
{
    [CustomEditor(typeof(MesoSpline), true)]
    public class MesoSplineEditor : Editor
    {
        static bool SplineSettingsFoldout = true;

        private static readonly string[] _dontIncludeMe = new string[] { "m_Script" };

        MesoSpline spline;
        int selectedCPIdx = -1;

        override public void OnInspectorGUI()
        {
            spline = (MesoSpline)this.target;
            if (spline == null)
                return;

            string splineTypeName = spline.GetType().BaseType.Name;
            if (splineTypeName.EndsWith("Spline"))
            {
                splineTypeName = splineTypeName.Remove(splineTypeName.Length - "Spline".Length);
            }

            //DrawDefaultInspector();
            EditorGUILayout.Separator();
            SplineSettingsFoldout = EditorGUILayout.Foldout(SplineSettingsFoldout, splineTypeName + " Settings");
            if (SplineSettingsFoldout)
            {

                EditorGUI.BeginChangeCheck();
                EditorGUILayout.BeginVertical(new GUIStyle());
                //Same as DrawDefaultInspector(); but excludes the script field
                serializedObject.Update();
                DrawPropertiesExcluding(serializedObject, _dontIncludeMe);
                serializedObject.ApplyModifiedProperties();

                setDirty(EditorGUI.EndChangeCheck());

                if (spline.ParentFork != null)
                {
                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.ObjectField("Forked from", spline.ParentFork.ParentSpline, typeof(CubicHermiteSpline), true);
                    EditorGUI.EndDisabledGroup();
                }
                MesoSpline prevSpline = (MesoSpline)EditorGUILayout.ObjectField("Previous", spline.PreviousSpline, typeof(CubicHermiteSpline), true);
                EditorGUILayout.LabelField("Base Size", spline.BaseSize.ToString());
                if (prevSpline != spline.PreviousSpline && prevSpline != spline)
                {
                    Undo.RecordObject(spline, "Set previous spline for " + spline.name);
                    spline.PreviousSpline = prevSpline;
                    setDirty(true);
                }

                setDirty(drawCheckPointInspector());

                EditorGUI.BeginChangeCheck();
                spline.NextBaseSize = EditorGUILayout.FloatField("Next Size", spline.NextBaseSize);
                if (EditorGUI.EndChangeCheck())
                {
                    setDirty(true);
                }

                MesoSpline nextSpline = (MesoSpline)EditorGUILayout.ObjectField("Next", spline.NextSpline, typeof(CubicHermiteSpline), true);
                if (nextSpline != spline.NextSpline && nextSpline != spline)
                {
                    Undo.RecordObject(spline, "Set next spline for " + spline.name);
                    spline.NextSpline = nextSpline;
                    setDirty(true);
                }

                EditorGUILayout.LabelField("Approx. Length", spline.ApproximateTotalLength.ToString() + "m");

                EditorGUILayout.EndVertical();
            }

        }

        void setDirty(bool dirty = true)
        {
            if (dirty && spline != null)
            {
                EditorUtility.SetDirty(spline);
                spline.notifyChanges();
            }
        }

        void OnDisable()
        {
            if(spline != null)
            {
                spline.HighlightIdx = -1;
            }
        }

        public void OnSceneGUI()
        {
            spline = (MesoSpline)this.target;
            if (spline == null)
                return;

            for (int i = 1; i < spline.ControlPoints.Count; i++)
            {
                CubicHermiteSpline.ControlPoint cp = spline.ControlPoints[i];

                if (selectedCPIdx != -1)
                {
                    if (selectedCPIdx == i)
                    {
                        cp.Position = spline.transform.InverseTransformPoint(Handles.PositionHandle(spline.transform.TransformPoint(cp.Position), Quaternion.identity));
                    }
                }
                else
                {
                    cp.Position = spline.transform.InverseTransformPoint(Handles.PositionHandle(spline.transform.TransformPoint(cp.Position), Quaternion.identity));
                }

                if (GUI.changed)
                {
                    Undo.RecordObject(spline, "Translate " + spline.name + ":CP#" + i);
                    spline.ControlPoints[i] = cp;
                    setDirty();
                }
            }

        }

        bool drawCheckPointInspector()
        {
            bool hasChanged = false;
            Color bgColor = GUI.backgroundColor;
            Color color = GUI.color;

            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();

            EditorGUI.BeginDisabledGroup(true);
            GUILayout.Button("Control Point", GUI.skin.label);
            GUI.backgroundColor = Handles.xAxisColor;
            EditorGUILayout.TextField("X");
            GUI.backgroundColor = Handles.yAxisColor;
            EditorGUILayout.TextField("Y");
            GUI.backgroundColor = Handles.zAxisColor;
            EditorGUILayout.TextField("Z");
            GUI.backgroundColor = bgColor;
            EditorGUILayout.TextField("π");
            EditorGUI.EndDisabledGroup();

            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < spline.ControlPoints.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                CubicHermiteSpline.ControlPoint cp = spline.ControlPoints[i];

                bgColor = GUI.backgroundColor;
                GUILayout.Space(50);
                if (selectedCPIdx == i)
                    GUI.color = Handles.selectedColor;
                if (GUILayout.Button("   #" + i, GUI.skin.label))
                {
                    if (selectedCPIdx == i)
                    {
                        spline.HighlightIdx = -1;
                        selectedCPIdx = -1;
                    }
                    else
                    {
                        spline.HighlightIdx = i;
                        selectedCPIdx = i;
                    }

                    SceneView.RepaintAll();
                }

                EditorGUI.BeginDisabledGroup(i == 0);

                EditorGUI.BeginChangeCheck();
                GUI.backgroundColor = Handles.xAxisColor;
                cp.Position.x = EditorGUILayout.FloatField(cp.Position.x);
                GUI.backgroundColor = Handles.yAxisColor;
                cp.Position.y = EditorGUILayout.FloatField(cp.Position.y);
                GUI.backgroundColor = Handles.zAxisColor;
                cp.Position.z = EditorGUILayout.FloatField(cp.Position.z);
                EditorGUI.EndDisabledGroup();
                GUI.backgroundColor = bgColor;
                //EditorGUI.BeginDisabledGroup(i == 0 && spline.PreviousSpline != null);
                cp.Radius = EditorGUILayout.Slider(cp.Radius, 0f, 10f);
                //EditorGUI.EndDisabledGroup();
                GUI.color = color;
                if(EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(spline, "Translate " + spline.name + ":CP#" + i);
                    spline.ControlPoints[i] = cp;
                    hasChanged = true;
                }


                EditorGUILayout.EndHorizontal();
            }


            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.PrefixLabel("" + spline.ControlPoints.Count+" Control Points");
            if (GUILayout.Button("Add Control Point"))
            {
                Undo.RecordObject(spline, "Add CP to " + spline.name);
                spline.addControlPoint();
                hasChanged = true;
            }

            EditorGUILayout.EndHorizontal();

            if (selectedCPIdx > 0 && Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Delete)
            {
                if (spline.ControlPoints.Count > selectedCPIdx)
                {
                    Undo.RecordObject(spline, "Delete CP from " + spline.name);
                    spline.ControlPoints.RemoveAt(selectedCPIdx);
                    selectedCPIdx = -1;
                    spline.HighlightIdx = -1;
                    hasChanged = true;
                }
            }

            EditorGUILayout.EndVertical();
            return hasChanged;
        }
    }
}