﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;

namespace SMS.Behaviours
{
    [CustomEditor(typeof(ForkPoint), true)]
    public class ForkPointEditor : Editor
    {

        private static readonly string[] _dontIncludeMe = new string[] { "m_Script" };
        ForkPoint fp;

        override public void OnInspectorGUI()
        {

            fp = (ForkPoint)this.target;
            if (fp == null)
                return;

            EditorGUI.BeginChangeCheck();

            fp.enabled = EditorGUILayout.Toggle("Enabled", fp.enabled);
            EditorGUILayout.Separator();

            fp.AttachedSpline = (MesoSpline)EditorGUILayout.ObjectField("Attach:", fp.AttachedSpline, typeof(MesoSpline), true);
            fp.ParentSpline = (MesoSpline)EditorGUILayout.ObjectField("On:",fp.ParentSpline, typeof(MesoSpline), true);


            serializedObject.Update();

            DrawPropertiesExcluding(serializedObject, _dontIncludeMe);

            serializedObject.ApplyModifiedProperties();


            if (EditorGUI.EndChangeCheck())
            {
                if (fp.ParentSpline != null)
                {
                    fp.ParentSpline.notifyChanges();
                }
                if (fp.AttachedSpline != null)
                {
                    fp.AttachedSpline.notifyChanges();
                }
            }
        }
    }
}