﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;

namespace SMS.Behaviours.Features.Core
{
    [CustomEditor(typeof(ForkFeature), true)]
    public class ForkFeatureEditor : Editor
    {

        ForkFeature fp;

        override public void OnInspectorGUI()
        {
            fp = (ForkFeature)this.target;
            if (fp == null)
                return;

            EditorGUI.BeginChangeCheck();

            fp.enabled = EditorGUILayout.Toggle("Enabled", fp.enabled);
            EditorGUILayout.Separator();

            fp.AttachedSpline = (MesoSpline)EditorGUILayout.ObjectField(fp.AttachedSpline, typeof(MesoSpline), true);
            DrawDefaultInspector();


            if (EditorGUI.EndChangeCheck())
            {
                /** Irrelevant
                if(fp.MesoFP != null && fp.MesoFP.BoundSpline != null)
                {
                    fp.MesoFP.BoundSpline.notifyChanges();
                }
                **/
                if(fp.AttachedSpline != null)
                {
                    fp.AttachedSpline.notifyChanges();
                }
            }
        }
    }
}