﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;

namespace SMS.Behaviours
{
    [CustomEditor(typeof(MesoMesh), true)]
    public class MesoMeshEditor : Editor
    {

        private static readonly string[] _dontIncludeMe = new string[] { "m_Script" };

        MesoMesh mesh;

        override public void OnInspectorGUI()
        {
            mesh = (MesoMesh)this.target;
            if (mesh == null)
                return;

            //Same as DrawDefaultInspector(); but excludes the script field
            serializedObject.Update();

            DrawPropertiesExcluding(serializedObject, _dontIncludeMe);

            serializedObject.ApplyModifiedProperties();
            
            EditorGUILayout.Separator();

            EditorGUILayout.LabelField("Recreate Mesh");

            if (GUILayout.Button("Recreate Using Settings"))
            {
                mesh.forceMeshUpdate();
            }

            EditorGUILayout.BeginHorizontal();

            bool previousSetting = mesh.GenerateFeatureTransform;
            if (GUILayout.Button("Base Form"))
            {
                mesh.GenerateFeatureTransform = false;
                mesh.forceMeshUpdate();
            }
            if (GUILayout.Button("Combined Form"))
            {
                mesh.GenerateFeatureTransform = true;
                mesh.forceMeshUpdate();
            }
            mesh.GenerateFeatureTransform = previousSetting;

            EditorGUILayout.EndHorizontal();


            EditorGUILayout.Separator();

            EditorGUILayout.LabelField("Mesh Statistics");
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.BeginVertical();

            if(mesh.GeneratedMesh != null)
            {
                EditorGUILayout.LabelField("Verts", mesh.GeneratedMesh.vertexCount.ToString());
                EditorGUILayout.LabelField("Tris", (mesh.GeneratedMesh.triangles.Length / 3).ToString());
            }
            EditorGUI.EndDisabledGroup();
            EditorGUILayout.EndVertical();
        }
    }
}