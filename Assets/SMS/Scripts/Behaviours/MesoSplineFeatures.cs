﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;
using SMS.Features;
using SMS.MeshGeneration;

namespace SMS.Behaviours
{
    [ExecuteInEditMode]
    public class MesoSplineFeatures : MonoBehaviour, FeatureContainer
    {
        public MesoSpline Spline;

        void Update()
        {
            //Re-sort the features in hierarchy
            List<MesoSplineFeaturePoint> sorted = getMesoSplineFeaturePoints();
            int i = 0;
            foreach (MesoSplineFeaturePoint fp in sorted)
            {
                if (fp.transform.GetSiblingIndex() != i)
                {
                    fp.transform.SetSiblingIndex(i);
                }
                i++;
            }

        }

        public List<MesoSplineFeaturePoint> getMesoSplineFeaturePoints(bool onlyActive = false)
        {
            List<MesoSplineFeaturePoint> fps = new List<MesoSplineFeaturePoint>();
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                MesoSplineFeaturePoint fp = child.GetComponent<MesoSplineFeaturePoint>();
                if (fp != null && (!onlyActive || (fp.enabled && fp.gameObject.activeSelf)))
                {
                    fps.Add(fp);
                }
            }
            fps.Sort((a, b) => { return a.T.CompareTo(b.T); });
            return fps;
        }

        #region Feature Pipeline

        public List<FeaturePoint> getFeaturePoints()
        {
            return getMesoSplineFeaturePoints().ConvertAll<FeaturePoint>(fp => (FeaturePoint)fp);
        }
        public List<FeaturePoint> getFeaturePoints(bool onlyActive)
        {
            return getMesoSplineFeaturePoints(onlyActive).ConvertAll<FeaturePoint>(fp => (FeaturePoint)fp);
        }
        public List<FeaturePoint> getFeaturePointsInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
            List<FeaturePoint> fps = getFeaturePoints(true);
            List<FeaturePoint> fpsInRange = new List<FeaturePoint>();
            foreach (FeaturePoint fp in fps)
            {
                foreach (FeatureDescriptor desc in fp.getDescriptors())
                {
                    if (desc.isInCharge(t, transformedPoint, originPoint, spline))
                    {
                        fpsInRange.Add(fp);
                        break;
                    }
                }
            }
            return fpsInRange;
        }
        public List<FeatureDescriptor> transformPoint(float t, ref MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
            List<FeaturePoint> fpsInRange = getFeaturePointsInCharge(t, transformedPoint, originPoint, spline);
            List<FeatureDescriptor> usedDescriptors = new List<FeatureDescriptor>();
            foreach (FeaturePoint fp in fpsInRange)
            {
                usedDescriptors.AddRange(fp.transformPoint(t, ref transformedPoint, originPoint, spline));
            }
            return usedDescriptors;
        }

        #endregion
    }
}