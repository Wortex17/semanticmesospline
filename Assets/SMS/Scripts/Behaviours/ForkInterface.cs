﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SMS.Behaviours;

namespace SMS.Behaviours
{
    public interface ForkInterface
    {
        MesoSpline AttachedSpline
        {
            get;
            set;
        }

        MesoSpline ParentSpline
        {
            get;
        }

        float T
        {
            get;
        }

        float getSizeForForkedSpline();
        Vector3 getPositionForForkedSpline();
        Quaternion getRotationForForkedSpline();
    }
}