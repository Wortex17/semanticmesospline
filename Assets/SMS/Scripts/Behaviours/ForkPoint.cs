﻿using UnityEngine;
using System.Collections;

using FeatureParameter = SMS.Features.FeatureParameter;

namespace SMS.Behaviours
{
    [ExecuteInEditMode]
    public class ForkPoint : MonoBehaviour, ForkInterface
    {
        public FeatureParameter Size
        {
            get
            {
                return mSize;
            }
        }

        public FeatureParameter Orientation
        {
            get
            {
                return mOrientation;
            }
        }
        public FeatureParameter Tilt
        {
            get
            {
                return mTilt;
            }
        }

        public MesoSpline AttachedSpline
        {
            get
            {
                return mAttachedSpline;
            }
            set
            {
                if (mAttachedSpline != value)
                {
                    if (mAttachedSpline != null)
                    {
                        mAttachedSpline.ParentForkPoint = null;
                    }
                    mAttachedSpline = value;
                    if (mAttachedSpline != null)
                    {
                        mAttachedSpline.ParentForkPoint = this;
                    }
                }
            }
        }

        public MesoSpline ParentSpline
        {
            get
            {
                return mParentSpline;
            }
            set
            {
                mParentSpline = value;
            }
        }

        public float T
        {
            get
            {
                return mT;
            }
            set
            {
                mT = Mathf.Clamp01(value);
            }
        }

        public float getSizeForForkedSpline()
        {
            return Size.getValue(this);
        }

        public Vector3 getPositionForForkedSpline()
        {
            if (ParentSpline == null)
                return this.transform.position;
            return ParentSpline.getGlobalPositionAt(T);
        }

        public Quaternion getRotationForForkedSpline()
        {
            if (ParentSpline == null)
                return this.transform.rotation;

            MesoSpline boundSpline = ParentSpline;

            Vector3 normal = Math.ParallelTransport.getNormalAt(boundSpline, T);

            //Get the tangent of the fork point
            Vector3 tangent = boundSpline.get1stDerivativeAt(T).normalized;

            // Gives us the sway along a horizontal disk, centered and aligned to the tangent
            float horizontalOrientation = Mathf.Repeat(Orientation.getValue(this.gameObject), 1f);
            //Rotate tangent by horizontalOrientation around normal
            tangent = Quaternion.AngleAxis(horizontalOrientation * 360f, normal) * tangent;

            // Gives us the sway along a vertical disk, centered and aligned to the normal
            float verticalOrientation = Mathf.Repeat(Tilt.getValue(this.gameObject), 1f);
            Vector3 binormal = Vector3.Cross(normal, tangent);
            //Rotate tangent by verticalOrientation around binormal
            tangent = Quaternion.AngleAxis(verticalOrientation * 360f, binormal) * tangent;


            //Transform the tangent into world space
            Vector3 tangentInWorld = boundSpline.transform.TransformDirection(tangent);

            //Use The worlds right/X axis be our reference axis
            Quaternion rotation = Quaternion.FromToRotation(Vector3.right, tangentInWorld);



            return rotation;
        }


        #region Unity Messages

        void Update()
        {
            if (ParentSpline != null)
            {
                Vector3 parentPos = ParentSpline.getGlobalPositionAt(T);
                if (transform.position != parentPos)
                {
                    transform.position = parentPos;
                }
            }
        }

        #endregion

        [SerializeField, HideInInspector]
        protected MesoSpline mParentSpline = null;
        [SerializeField, Range(0, 1)]
        protected float mT = 0f;
        [SerializeField, HideInInspector]
        protected MesoSpline mAttachedSpline = null;

        [SerializeField]
        protected FeatureParameter mSize = new SMS.Features.FeatureParameter(0f);
        [SerializeField]
        protected FeatureParameter mOrientation = new SMS.Features.FeatureParameter(0f);
        [SerializeField]
        protected FeatureParameter mTilt = new SMS.Features.FeatureParameter(0f);
    }
}