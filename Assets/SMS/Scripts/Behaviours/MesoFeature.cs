﻿using UnityEngine;
using System.Collections;

using SMS.Features;
using SMS.MeshGeneration;

namespace SMS.Behaviours
{
    [RequireComponent(typeof(MesoSplineFeaturePoint))]
    public abstract class MesoFeature : MonoBehaviour, FeatureDescriptor
    {
        public FeaturePoint FP
        {
            get
            {
                return (FeaturePoint)this.GetComponent<MesoSplineFeaturePoint>();
            }
        }

        public MesoSplineFeaturePoint MesoFP
        {
            get
            {
                return this.GetComponent<MesoSplineFeaturePoint>();
            }
        }

        void OnValidate()
        {
            if (Application.isEditor && !Application.isPlaying)
            {
                //Update the mesh
                if (MesoFP.BoundSpline != null)
                {
                    MesoFP.BoundSpline.notifyChanges();
                }
            }
        }

        public abstract bool isInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline);
        public abstract void transformPoint(float t, ref MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline);
    }
}