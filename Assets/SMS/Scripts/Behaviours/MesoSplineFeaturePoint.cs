﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;
using SMS.Features;
using SMS.MeshGeneration;

namespace SMS.Behaviours
{
    /// <summary>
    /// A MesoSplineFeaturePoint is meant to be a child of a MesoSplineFeatures Behaviour
    /// It places itself according to the assigned T value
    /// </summary>
    [ExecuteInEditMode]
    public class MesoSplineFeaturePoint : MonoBehaviour, FeaturePoint
    {
        /// The linked parent container
        public MesoSplineFeatures Parent
        {
            get
            {
                return transform.parent.GetComponent<MesoSplineFeatures>();
            }
        }

        //If this feature is validly bound to a spline
        public bool IsBound
        {
            get
            {
                return (Parent != null && Parent.Spline != null);
            }
        }

        public MesoSpline BoundSpline
        {
            get
            {
                if (Parent != null)
                    return Parent.Spline;
                return null;
            }
        }

        public float T
        {
            get
            {
                return mT.getValue(this);
            }
        }

        public MesoFeature[] getMesoFeatures()
        {
            return gameObject.GetComponents<MesoFeature>();
        }

        #region Feature Pipeline

        public FeatureDescriptor[] getDescriptors()
        {
            return gameObject.GetComponents<MesoFeature>();
        }
        public List<FeatureDescriptor> getDecriptorsInCharge(float t, MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
            MesoFeature[] descriptors = getMesoFeatures();
            List<FeatureDescriptor> descriptorsInRange = new List<FeatureDescriptor>();
            foreach (MesoFeature descriptor in descriptors)
            {
                if (descriptor.enabled && descriptor.isInCharge(t, transformedPoint, originPoint, spline))
                {
                    descriptorsInRange.Add(descriptor);
                }
            }
            return descriptorsInRange;
        }
        public List<FeatureDescriptor> transformPoint(float t, ref MeshPointDescriptor transformedPoint, MeshPointDescriptor originPoint, SMS.Math.CubicHermiteSpline spline)
        {
            List<FeatureDescriptor> descriptorsInRange = getDecriptorsInCharge(t, transformedPoint, originPoint, spline);
            foreach (FeatureDescriptor descriptor in descriptorsInRange)
            {
                descriptor.transformPoint(t, ref transformedPoint, originPoint, spline);
            }
            return descriptorsInRange;
        }

        #endregion

        [SerializeField]
        public SMS.Features.FeatureParameter mT = new SMS.Features.FeatureParameter(0f);

        #region Unity Messages

        void Update()
        {
            if (IsBound)
            {
                //Reposition if necessary
                Vector3 position = BoundSpline.getGlobalPositionAt(T);
                if (position != transform.position)
                {
                    transform.position = position;
                    BoundSpline.notifyChanges();
                }
            }
        }

        #endregion
    }
}