﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SMS.Semantics
{

    /// <summary>
    /// The Context Child Layer contains Context Parameter Modifiers and is a level of its Virtual Context Inheritance Hierarchy
    /// </summary>
    public class ContextChildLayer : ContextLayer
    {

        public float getOffset(string name)
        {
            ContextParameterModifier param = getModifier(name);
            if (param == null)
                return ContextParameterModifier.DEFAULT_OFFSET;
            else return param.Offset;
        }

        /// <summary>
        /// Changes the Offset of a Modifier or creates a new Modifier with that Offset.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="Offset"></param>
        /// <returns>True if a Offset was changed or a param was created</returns>
        public bool setModifierOffset(string name, float Offset)
        {
            ContextParameterModifier param = getModifier(name);
            if (param == null)
            {
                mModifiers.Add(new ContextParameterModifier(name, Offset));
                return true;
            }
            else if (param.Offset != Offset)
            {
                param.Offset = Offset;
                return true;
            }

            return false;
        }
        public bool renameModifier(string name, string newname)
        {
            if (name == newname)
                return false;

            ContextParameterModifier param = getModifier(name);
            if (param != null && !hasModifier(newname))
            {
                param.Name = newname;
                return true;
            }

            return false;
        }
        public bool removeModifier(string name)
        {
            ContextParameterModifier param = getModifier(name);
            if (param != null)
            {
                return mModifiers.Remove(param);
            }

            return false;
        }

        public bool hasModifier(string name)
        {
            return getModifier(name) != null;
        }
        public ContextParameterModifier getModifier(string name)
        {
            return mModifiers.Find((mod) => mod.Name == name);
        }
        public List<string> listModifierNames()
        {
            return mModifiers.ConvertAll<string>((mod) => mod.Name);
        }

        /// <summary>
        /// Use a List instead of Dict or other uniques
        /// to leverage unitys serializer
        /// </summary>
        [SerializeField]
        List<ContextParameterModifier> mModifiers = new List<ContextParameterModifier>();

    }

}