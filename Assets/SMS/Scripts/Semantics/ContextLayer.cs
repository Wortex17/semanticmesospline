﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SMS.Semantics
{

    /// <summary>
    /// A base class for Context Layers (Root and Child)
    /// </summary>
    public abstract class ContextLayer : MonoBehaviour
    {

        /// <summary>
        /// Generates the Modified Context Value
        /// Defaults to ContextParameter.DEFAULT_VALUE
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static float getModifiedContextValueAt(Transform target, string parameterName)
        {
            ContextRootLayer root = getContextRootOf(target);
            if (root == null)
                return ContextParameter.DEFAULT_VALUE;

            float value = root.getValue(parameterName);

            foreach (ContextChildLayer child in getContextModifierChainUntil(target))
            {
                value = Mathf.Clamp01(value + child.getOffset(parameterName));
            }

            return value;

        }

        public static ContextLayer getClosestContextLayerTo(Transform target)
        {
            return target.GetComponentInParent<ContextLayer>();
        }
        public static ContextRootLayer getContextRootOf(Transform target)
        {
            return target.GetComponentInParent<ContextRootLayer>();
        }
        public static ContextChildLayer getClosestContextChildTo(Transform target)
        {
            return target.GetComponentInParent<ContextChildLayer>();
        }

        /// <summary>
        /// Returns the context inheritance chain up to this layer (all ContextChildLayers)
        /// </summary>
        /// <returns></returns>
        public static List<ContextChildLayer> getContextModifierChainUntil(Transform target)
        {
            ContextRootLayer root = getContextRootOf(target);
            List<ContextChildLayer> chain = new List<ContextChildLayer>();

            Transform head = target;
            while (head != null && head.transform != root.transform)
            {
                ContextChildLayer child = head.GetComponent<ContextChildLayer>();
                if (child != null)
                    chain.Add(child);
                head = head.parent;
            }

            chain.Reverse();
            return chain;
        }

        public ContextRootLayer getContextRoot()
        {
            return getContextRootOf(this.transform);
        }

    }

}