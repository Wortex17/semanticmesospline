﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;

namespace SMS.Semantics
{
    
    [CustomEditor(typeof(ContextChildLayer), true)]
    public class ContextChildLayerEditor : Editor
    {
        ContextChildLayer layer;
        ContextRootLayer root;
        private static readonly string[] _dontIncludeMe = new string[] { "m_Script", "mModifiers" };

        ContextParameterModifier newMod = new ContextParameterModifier("New Modifier");

        override public void OnInspectorGUI()
        {
            layer = (ContextChildLayer)this.target;
            if (layer == null)
                return;

            root = ContextLayer.getContextRootOf(layer.transform);
            if (root == null)
            {
                Color cColor = GUI.contentColor;
                GUI.contentColor = Color.yellow;
                EditorGUILayout.LabelField("No Context Root detected");
                GUI.contentColor = cColor;
            }

            //Same as DrawDefaultInspector(); but excludes the script field
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, _dontIncludeMe);
            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.Separator();

            if (drawModEditor())
            {
                EditorUtility.SetDirty(layer);
                foreach (Behaviours.MesoSpline spline in layer.GetComponentsInChildren<Behaviours.MesoSpline>())
                {
                    spline.notifyChanges();
                }
            }
            
        }

        bool drawModEditor()
        {
            bool changes = false;

            EditorGUILayout.LabelField("Context Parameter Modifiers");

            Color cColor = GUI.contentColor;
            Color bgColor = GUI.backgroundColor;
            EditorGUILayout.BeginVertical();

            foreach (string modName in layer.listModifierNames())
            {
                string currentModName = modName;
                float offset = layer.getOffset(currentModName);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.BeginVertical();

                //Default Edit
                EditorGUILayout.BeginHorizontal();

                if (!root.hasParameter(currentModName))
                {
                    GUI.contentColor = Color.yellow;
                }

                string newModName = EditorGUILayout.TextField(currentModName);
                if (layer.renameModifier(modName, newModName))
                {
                    currentModName = newModName;
                    changes = true;
                }
                GUI.contentColor = cColor;

                if (layer.setModifierOffset(currentModName, EditorGUILayout.Slider(offset, -1f, 1f)))
                    changes = true;

                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("X"))
                {
                    if (layer.removeModifier(currentModName))
                        changes = true;
                }
                GUI.backgroundColor = bgColor;

                EditorGUILayout.EndHorizontal();
                //Value Preview
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.PrefixLabel("Current MCV:");
                EditorGUILayout.Space();
                float hereValue = ContextLayer.getModifiedContextValueAt(layer.transform, currentModName);
                if (layer.transform.parent != null && ContextLayer.getClosestContextChildTo(layer.transform.parent) != null)
                {
                    float parentValue = ContextLayer.getModifiedContextValueAt(layer.transform.parent, currentModName);
                    EditorGUILayout.SelectableLabel(root.getValue(currentModName) + " -> " + parentValue.ToString() + " -> " + hereValue.ToString());
                }
                else
                {
                    EditorGUILayout.SelectableLabel("       " + root.getValue(currentModName) + " -> " + hereValue.ToString());
                }


                EditorGUILayout.EndHorizontal();

                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();

            }
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Add new Modifier");

            //Field for creating new Param

            EditorGUILayout.BeginHorizontal();

            bool conflicting = false;

            if (layer.hasModifier(newMod.Name))
            {
                GUI.contentColor = Color.red;
                conflicting = true;
            }
            else if (!root.hasParameter(newMod.Name))
            {
                GUI.contentColor = Color.yellow;
            }
            newMod.Name = EditorGUILayout.TextField(newMod.Name);
            GUI.contentColor = cColor;

            newMod.Offset = EditorGUILayout.Slider(newMod.Offset, -1f, 1f);

            EditorGUI.BeginDisabledGroup(conflicting);
            GUI.backgroundColor = Color.green;
            if (GUILayout.Button("+"))
            {
                if (layer.setModifierOffset(newMod.Name, newMod.Offset))
                {
                    changes = true;
                }
            }
            EditorGUI.EndDisabledGroup();

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
            GUI.contentColor = cColor;
            GUI.backgroundColor = bgColor;

            return changes;
        }
    }
}