﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using SMS.Math;

namespace SMS.Semantics
{
    
    [CustomEditor(typeof(ContextRootLayer), true)]
    public class ContextRootLayerEditor : Editor
    {

        ContextRootLayer layer;
        private static readonly string[] _dontIncludeMe = new string[] { "m_Script", "mParameters" };

        ContextParameter newParam = new ContextParameter("New Parameter");

        override public void OnInspectorGUI()
        {
            layer = (ContextRootLayer)this.target;
            if (layer == null)
                return;

            //Same as DrawDefaultInspector(); but excludes the script field
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, _dontIncludeMe);
            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.Separator();

            if (drawParamEditor())
            {
                EditorUtility.SetDirty(layer);
                foreach (Behaviours.MesoSpline spline in layer.GetComponentsInChildren<Behaviours.MesoSpline>())
                {
                    spline.notifyChanges();
                }
            }
            
        }

        bool drawParamEditor()
        {
            bool changes = false;

            EditorGUILayout.LabelField("Context Parameters");

            Color cColor = GUI.contentColor;
            Color bgColor = GUI.backgroundColor;
            EditorGUILayout.BeginVertical();

            foreach (string paramName in layer.listParameterNames())
            {
                string currentParamName = paramName;
                float paramValue = layer.getValue(currentParamName);

                EditorGUILayout.BeginHorizontal();

                string newParamName = EditorGUILayout.TextField(currentParamName);
                if (layer.renameParameter(currentParamName, newParamName))
                {
                    currentParamName = newParamName;
                    changes = true;
                }
                if (layer.setParameterValue(currentParamName, EditorGUILayout.Slider(paramValue, 0f, 1f)))
                    changes = true;

                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("X"))
                {
                    if (layer.removeParameter(currentParamName))
                        changes = true;
                }
                GUI.backgroundColor = bgColor;

                EditorGUILayout.EndHorizontal();

            }
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Add new Parameter");

            //Field for creating new Param

            EditorGUILayout.BeginHorizontal();

            bool conflicting = false;

            if (layer.hasParameter(newParam.Name))
            {
                GUI.contentColor = Color.red;
                conflicting = true;
            }
            newParam.Name = EditorGUILayout.TextField(newParam.Name);
            GUI.contentColor = cColor;

            newParam.Value = EditorGUILayout.Slider(newParam.Value, 0f, 1f);

            EditorGUI.BeginDisabledGroup(conflicting);
            GUI.backgroundColor = Color.green;
            if (GUILayout.Button("+"))
            {
                if (layer.setParameterValue(newParam.Name, newParam.Value))
                {
                    changes = true;
                }
            }
            EditorGUI.EndDisabledGroup();

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
            GUI.contentColor = cColor;
            GUI.backgroundColor = bgColor;

            return changes;
        }
    }
}