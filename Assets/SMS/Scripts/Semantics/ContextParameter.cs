﻿using UnityEngine;
using System.Collections;

namespace SMS.Semantics
{
    [System.Serializable]
    public class ContextParameter
    {
        public const float DEFAULT_VALUE = 0.5f;

        public ContextParameter(string name = "", float value = DEFAULT_VALUE)
        {
            Name = name;
            Value = value;
        }

        public string Name
        {
            get
            {
                return mName;
            }
            set
            {
                mName = value;
            }
        }

        public float Value
        {
            get
            {
                return mValue;
            }
            set
            {
                mValue = Mathf.Clamp01(value);
            }
        }

        [SerializeField]
        private string mName = "";
        [SerializeField, Range(0f, 1f)]
        private float mValue = DEFAULT_VALUE;
    }
}