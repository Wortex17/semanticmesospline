﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SMS.Semantics
{

    /// <summary>
    /// The Context Root Layer contains all Context Parameters and is the Root of its Virtual Context Inheritance Hierarchy
    /// </summary>
    public class ContextRootLayer : ContextLayer
    {

        public float getValue(string name)
        {
            ContextParameter param = getParameter(name);
            if (param == null)
                return ContextParameter.DEFAULT_VALUE;
            else return param.Value;
        }

        /// <summary>
        /// Changes the value of a parameter or creates a new parameter with that value.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns>True if a value was changed or a param was created</returns>
        public bool setParameterValue(string name, float value)
        {
            ContextParameter param = getParameter(name);
            if (param == null)
            {
                mParameters.Add(new ContextParameter(name, value));
                return true;
            }
            else if(param.Value != value)
            {
                param.Value = value;
                return true;
            }

            return false;
        }
        public bool renameParameter(string name, string newname)
        {
            if (name == newname)
                return false;

            ContextParameter param = getParameter(name);
            if (param != null && !hasParameter(newname))
            {
                param.Name = newname;
                return true;
            }

            return false;
        }
        public bool removeParameter(string name)
        {
            ContextParameter param = getParameter(name);
            if (param != null)
            {
                return mParameters.Remove(param);
            }

            return false;
        }

        public bool hasParameter(string name)
        {
            return getParameter(name) != null;
        }
        public ContextParameter getParameter(string name)
        {
            return mParameters.Find((param) => param.Name == name);
        }
        public List<string> listParameterNames()
        {
            return mParameters.ConvertAll<string>((param) => param.Name);
        }


        /// <summary>
        /// Use a List instead of Dict or other uniques
        /// to leverage unitys serializer
        /// </summary>
        [SerializeField]
        List<ContextParameter> mParameters = new List<ContextParameter>();
    }

}