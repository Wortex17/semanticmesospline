﻿using UnityEngine;
using System.Collections;

namespace SMS.Semantics
{
    [System.Serializable]
    public class ContextParameterModifier
    {
        public const float DEFAULT_OFFSET = 0.0f;

        public ContextParameterModifier(string name = "", float value = DEFAULT_OFFSET)
        {
            Name = name;
            Offset = value;
        }

        public string Name
        {
            get
            {
                return mName;
            }
            set
            {
                mName = value;
            }
        }

        public float Offset
        {
            get
            {
                return mOffset;
            }
            set
            {
                mOffset = Mathf.Clamp(value, -1f, 1f);
            }
        }

        [SerializeField]
        private string mName = "";
        [SerializeField, Range(-1f, 1f)]
        private float mOffset = DEFAULT_OFFSET;
    }
}